# Breathe analysis

This software analyzing respiratory trace by automatically identifying peaks and count BPM (breaths per minute), IPPM (inhalation peaks per minute) and laterality properties.

## Data
- For each patient, there is a mat file in the "Data" directory.
- Each file contains the following variables:
    - Code - The code of the participant, as group and index number.
    - Group - The group of the participant (Anosmic/Normosmic).
    - Gender - The gender of the participant (M/F).
    - Age - The age of the participant, in years.
    - SampleLength - The duration, in seconds, between each successive samples. We used two different sampling rates (0.18 sec, 1/6 sec)
    - SleepIndex - The index in the data in which the participant went to sleep.
    - WakeUpIndex - THe index in the data in which the participant woke up.
    - Data: Matrix of Nx2 samples, with the samples respiratory trace from two nostrils.
- All the metadata (all the above variables, except "Data") are presented also in the csv file ("Participants.csv")

## Contirbutors
- Date: July 04, 2023
- Version: 1.0
- Authors: 
    - Lior Gorodisky, Noam Sobel's Olfaction Lab, Weizmann Institute of Science
- Contact: lior.gorodisky@weizmann.ac.il

If you find this code useful please credit us in any papers/software published

