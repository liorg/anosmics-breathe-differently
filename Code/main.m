%% Load all the data
data_folder = '../Data/';
files_in_data_folder = dir([data_folder, '*.mat']);
loaded_data = cellfun(@(filename) load([data_folder, filename]), {files_in_data_folder.name}, 'UniformOutput', false);
data = [loaded_data{:}];

%% Create LPF for all data
for lpf_freq = [3, nan] % Hz
    
    %% For each participant, do all the analysis
    analysis_results = cell(size(data));
    start_time = tic;
    for i=1:numel(data)
        fprintf('Running all analysis for subject %s, starting at time %1.3f...\n', data(i).Code, toc(start_time));
        sample_length = data(i).SampleLength;
        if sample_length > 0.17
            sample_length = 0.18;
        else
            sample_length = 1/6;
        end
        
        current_freq = 1/sample_length;
        current_participant = data(i);
        
        if exist('lpf_freq', 'var') && ~isnan(lpf_freq)
            fprintf('Downsampling from %1.2f Hz to %1.2f Hz...\n', current_freq, lpf_freq);
            old_data = current_participant.Data;
            old_data(isnan(old_data)) = 0;
            old_timepoints = (1:size(old_data, 1))-1;
            new_timepoints = old_timepoints*current_freq/lpf_freq;
            new_timepoints(new_timepoints>old_timepoints(end)) = [];
            new_data = interp1(old_timepoints, old_data, new_timepoints,'spline');
            
            current_participant.SampleLength = 1/lpf_freq;
            current_participant.Data = new_data;
            current_participant.SleepIndex = round(lpf_freq/current_freq*current_participant.SleepIndex);
            current_participant.WakeUpIndex = round(lpf_freq/current_freq*current_participant.WakeUpIndex);
            if current_participant.WakeUpIndex > current_participant.SleepIndex
                if current_participant.SleepIndex > size(new_data, 1)
                    fprintf('Skipping current participant - there is no awake period.\n');
                    continue;
                end
                if current_participant.WakeUpIndex > size(new_data, 1)
                    current_participant.WakeUpIndex = size(new_data, 1);
                end
            else
                if current_participant.WakeUpIndex > size(new_data, 1)
                    error('What to do?');
                end
                if current_participant.SleepIndex > size(new_data, 1)
                    current_participant.SleepIndex = size(new_data, 1);
                end
            end
            if strcmp(current_participant.Code, 'Nor 10')
                fprintf('Skipping current participant - there is no awake period when averaging stats.\n');
            end
        end
        
        
        fprintf('Running nasal-cycle analysis...\n');
        nasalCycleResults = Analysis_NasalCycleForSubject(current_participant.Data, current_participant.SleepIndex, current_participant.WakeUpIndex, sample_length);
        
        fprintf('Running respiratory-features analysis...\n');
        
        respiratoryDataSplitted = GetDataSplitted(current_participant.Data, current_participant.SleepIndex, current_participant.WakeUpIndex, sample_length);
        respiratoryFeatures = RespiratoryFeaturesForSplittedData(respiratoryDataSplitted);
        respiratoryFeaturesMeans = MeanRespiratoryFeatureByArousalState(respiratoryFeatures);
        
        fprintf('Running peaks-finder analysis...\n');
        respiratoryPeaks = GetPeaksData(current_participant.Data, current_participant.SleepIndex, current_participant.WakeUpIndex, sample_length);
        
        fprintf('Running peaks analysis...\n');
        block_length_in_minutes = 5;
        WAKE_SLEEP_ENUM = struct('WAKE_ONLY', 0, 'SLEEP_ONLY', 1, 'WAKE_AND_SLEEP', 2);
        wake_sleep_options = [WAKE_SLEEP_ENUM.WAKE_ONLY, WAKE_SLEEP_ENUM.SLEEP_ONLY];
        max_peaks_in_minute = 40;
        min_peaks_in_minute = 5;
        peaksAnalysisPerSubjectAll = AnalysisPeaksPerSubject(respiratoryPeaks, ...
            block_length_in_minutes, min_peaks_in_minute, max_peaks_in_minute, ...
            WAKE_SLEEP_ENUM, wake_sleep_options);
        
        %% Store everything
        analysis_results{i}.NasalCycle = nasalCycleResults;
        analysis_results{i}.RespiratoryFeatures = respiratoryFeatures;
        analysis_results{i}.RespiratoryFeaturesMeans = respiratoryFeaturesMeans;
        analysis_results{i}.RespiratoryPeaks = respiratoryPeaks;
        analysis_results{i}.RespiratoryPeaksFeatures = [peaksAnalysisPerSubjectAll{:}];
        
    end
    fprintf('Finished running all analysis for all subjects in time %1.3f\n', toc(start_time));
    
    %% Merge everything
    removed_participants = cellfun(@isempty, analysis_results);
    analysis_results_concatenated = [analysis_results{:}];
    experiment_results = rmfield(data(~removed_participants), {'SampleLength', 'SleepIndex', 'WakeUpIndex', 'Data'});
    
    numRespiratoryFeaturesMeansFields = cellfun(@(item) numel(fieldnames(item)), {analysis_results_concatenated.RespiratoryFeaturesMeans});
    missing_data_participants = numRespiratoryFeaturesMeansFields < median(numRespiratoryFeaturesMeansFields);
    analysis_results_concatenated(missing_data_participants) = [];
    experiment_results(missing_data_participants) = [];
    
    % Merge Respiratory-features
    respiratory_features_concatenated = [analysis_results_concatenated.RespiratoryFeaturesMeans];
    respiratory_features_fields = fieldnames(respiratory_features_concatenated);
    for respiratory_features_field_index = 1:numel(respiratory_features_fields)
        current_field_name = respiratory_features_fields{respiratory_features_field_index};
        current_field_values = {respiratory_features_concatenated.(current_field_name)};
        if all(cellfun(@(values) isnumeric(values) && numel(values) == 1, current_field_values))
            [experiment_results.(current_field_name)] = deal(current_field_values{:});
        end
    end
    
    % Merge Nasal cycle
    nasal_cycle_concatenated = [analysis_results_concatenated.NasalCycle];
    nasal_cycle_fields = fieldnames(nasal_cycle_concatenated);
    for nasal_cycle_field_index = 1:numel(nasal_cycle_fields)
        current_field_name = nasal_cycle_fields{nasal_cycle_field_index};
        current_field_values = {nasal_cycle_concatenated.(current_field_name)};
        if all(cellfun(@(values) isnumeric(values) && numel(values) <= 1, current_field_values))
            [experiment_results.(current_field_name)] = deal(current_field_values{:});
        end
    end
    
    % Merge respiratory-peaks
    respiratory_peaks_concatenated = cat(1, analysis_results_concatenated.RespiratoryPeaksFeatures);
    inhalePeaksCountAvg = arrayfun(@(item) nanmean(item.InhalePeaksCount)/block_length_in_minutes, respiratory_peaks_concatenated, 'UniformOutput', false);
    
    [experiment_results.Respiration_peaks_count_avg_sleep] = deal(inhalePeaksCountAvg{:, wake_sleep_options == WAKE_SLEEP_ENUM.SLEEP_ONLY});
    [experiment_results.Respiration_peaks_count_avg_wake] = deal(inhalePeaksCountAvg{:, wake_sleep_options == WAKE_SLEEP_ENUM.WAKE_ONLY});
    
    %% Calculate moving-mean of BPM and IPPM per participant
    inhalePeaksCount = arrayfun(@(item) item.InhalePeaksCount'/block_length_in_minutes, respiratory_peaks_concatenated, 'UniformOutput', false);
    inhalePeaksBlocksStart = arrayfun(@(item) item.BlocksStarts'/60, respiratory_peaks_concatenated, 'UniformOutput', false);
    IPPM_sleep = inhalePeaksCount(:, wake_sleep_options == WAKE_SLEEP_ENUM.SLEEP_ONLY);
    IPPM_wake = inhalePeaksCount(:, wake_sleep_options == WAKE_SLEEP_ENUM.WAKE_ONLY);
    
    respiratoryFeaturesConcatenated = cat(1, analysis_results_concatenated.RespiratoryFeatures);
    ratesPerMinutesConcatenated = arrayfun(@(item) [item.Data.RatePerMinute], respiratoryFeaturesConcatenated, 'UniformOutput', false);
    arousalConcatenated = arrayfun(@(item) item.Arousal, respiratoryFeaturesConcatenated, 'UniformOutput', false);
    
    BPM_and_IPPM_per_block = struct('Code', {experiment_results.Code}', 'Group', {experiment_results.Group}', ...
        'IPPM_Wake', IPPM_wake, 'IPPM_Sleep', IPPM_sleep, ...
        'BPM_Wake', ratesPerMinutesConcatenated(strcmp(arousalConcatenated, 'Wake')), ...
        'BPM_Sleep', ratesPerMinutesConcatenated(strcmp(arousalConcatenated, 'Sleep')));
    
    %% Display moving-mean of BPM and IPPM by group
    block_length_in_minutes = 5;
    fields_to_display = {'BPM_Wake', 'BPM_Sleep', 'IPPM_Wake', 'IPPM_Sleep'};
    minimal_participans_required = 15;
    groups_colors = {[0.1, 0.7, 0.1], [1, 0.5, 0.25]};
    given_ylim = [0, 30];
    
    DisplayMovingMean(BPM_and_IPPM_per_block, fields_to_display, ...
        groups_colors, minimal_participans_required, block_length_in_minutes, 2, given_ylim);
    
    %% Display results
    fields_to_display = {{'Sleep_RatePerMinute', 'Wake_RatePerMinute'}, ...
        {'Respiration_peaks_count_avg_sleep', 'Respiration_peaks_count_avg_wake'}, ...
        {'Sleep_PercentBreathsWithInhalePause', 'Wake_PercentBreathsWithInhalePause'}, ...
        {'Sleep_COV_InhaleVolume', 'Wake_COV_InhaleVolume'}, ...
        {'Sleep_Exhale_value', 'Wake_Exhale_value'}, ...
        {'Mean_LI_sleep', 'Mean_LI_wake'}, ...
        {'Mean_amplitude_LI_sleep', 'Mean_amplitude_LI_wake'}, ...
        {'Nostril_corr_Sleep', 'Nostril_corr_Wake'}, ...
        {'Average_Interval_length_during_sleep', 'Average_Interval_length_during_wake'}};
    fields_prefixes = {'BPM during ', ...
        'IPPM during ', ...
        '% Breaths with inh. pause during ', ...
        'CoV of inhale volume during ', ...
        'Exhale peak value during ', ...
        'LI during ', ...
        'LI Amplitude during ', ...
        'Nostril corr. during ', 'Avg. interval length during '};
    
    groups_colors = {[0.1, 0.7, 0.1], [1, 0.5, 0.25]};
    
    sleep_vs_wake_figures = DisplaySleepVsWakePerGroup(experiment_results, fields_to_display, groups_colors, fields_prefixes);
    
    % Display raincloud figures
    groups_titles = {'Normosmic', 'Anosmic'};
    bars_x_ranges = {[-0.029, -0.012], [-0.05, -0.033]};
    raincloud_figures = DisplayRainclouds(experiment_results, ...
        fields_to_display, groups_titles, groups_colors, bars_x_ranges);
    
    % Merge raincloud and y-x figures
    raincloud_panel_width = 75;
    merged_figures = MergeFigures(sleep_vs_wake_figures, raincloud_figures, raincloud_panel_width);
    close([sleep_vs_wake_figures, raincloud_figures]);
    
    %% Write results to CSV
    experiment_results_to_save = experiment_results;
    % Remove irrelevant fields
    structs_fieldnames = fieldnames(experiment_results_to_save);
    fields_to_display_flat = [fields_to_display{:}]';
    for idx=1:numel(structs_fieldnames)
        current_fieldname = structs_fieldnames{idx};
        if ismember(current_fieldname, fields_to_display_flat)
            continue;
        end
        if ismember(current_fieldname, {'Code', 'Gender', 'Group'})
            continue;
        end
        current_field_values = {experiment_results_to_save.(current_fieldname)};
        if ischar(current_field_values{1})
            unique_values = unique(current_field_values);
            if numel(unique_values) == 1
                fprintf('Removing field with single value: %s\n', current_fieldname)
                experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
                continue;
            end
            fprintf('Removing field with strings: %s\n', current_fieldname)
            experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
            continue;
        end
        numel_per_field = cellfun(@numel, current_field_values);
        if any(numel_per_field > 1)
            fprintf('Removing field with vector(s): %s\n', current_fieldname)
            experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
        end
    end
    
    if exist('lpf_freq', 'var') && ~isnan(lpf_freq)
        hz_postfix = sprintf('_%d Hz', lpf_freq);
    else
        hz_postfix = '';
    end
    for option = 1:3
        if option == 1
            participants = true(size(experiment_results_to_save));
            file_postfix = '';
        elseif option == 2
            participants = strcmp({experiment_results_to_save.Group}, 'Normosmic');
            file_postfix = '_Normosmics';
        elseif option == 3
            participants = strcmp({experiment_results_to_save.Group}, 'Anosmic');
            file_postfix = '_Anosmics';
        end
        writetable(struct2table(experiment_results_to_save(participants)), ['Respiration_All_Results', file_postfix, hz_postfix, '.csv']);
    end
end

%% IPPM outliers check
IPPM_wake_measurements = [experiment_results.Respiration_peaks_count_avg_wake];
groups = {experiment_results.Group};

IPPM_anosmics = IPPM_wake_measurements(strcmp(groups, 'Anosmic'));
IPPM_normosmics = IPPM_wake_measurements(strcmp(groups, 'Normosmic'));

IPPM_anosmics_mean = mean(IPPM_anosmics);
IPPM_anosmics_std = std(IPPM_anosmics);
IPPM_anosmics_t_values = (IPPM_anosmics - IPPM_anosmics_mean) / IPPM_anosmics_std;

IPPM_normosmics_mean = mean(IPPM_normosmics);
IPPM_normosmics_std = std(IPPM_normosmics);
IPPM_normosmics_t_values = (IPPM_normosmics - IPPM_normosmics_mean) / IPPM_normosmics_std;

[~, IPPM_ttest_p, ~, IPPM_ttest_stats] = ttest2(IPPM_anosmics, IPPM_normosmics);

IPPM_normosmics_without_highest = setdiff(IPPM_normosmics, max(IPPM_normosmics));
[~, IPPM_without_highest_ttest_p, ~, IPPM_without_highest_ttest_stats] = ttest2(IPPM_anosmics, IPPM_normosmics_without_highest);

fprintf('T-values range for anosmics: %1.3f to %1.3f.\n', min(IPPM_anosmics_t_values), max(IPPM_anosmics_t_values));
fprintf('T-values range for normosmics: %1.3f to %1.3f.\n', min(IPPM_normosmics_t_values), max(IPPM_normosmics_t_values));
fprintf('Highest IPPM: %1.3f (%s)\n', max(IPPM_wake_measurements), experiment_results(IPPM_wake_measurements == max(IPPM_wake_measurements)).Code);
fprintf('Normosmics stats without highest IPPM: %1.1f +- %1.1f.\n', nanmean(IPPM_normosmics_without_highest), nanstd(IPPM_normosmics_without_highest));
fprintf('Stats with all participants: t(%d)=%1.2f, d''=%1.2f, p = %1.3f\n', ...
    IPPM_ttest_stats.df, IPPM_ttest_stats.tstat, ...
    (mean(IPPM_normosmics)-mean(IPPM_anosmics))/IPPM_ttest_stats.sd, IPPM_ttest_p);
fprintf('Stats without higheset IPPM: t(%d)=%1.2f, d''=%1.2f, p = %1.3f\n', ...
    IPPM_without_highest_ttest_stats.df, IPPM_without_highest_ttest_stats.tstat, ...
    (mean(IPPM_normosmics_without_highest)-mean(IPPM_anosmics))/IPPM_without_highest_ttest_stats.sd, IPPM_without_highest_ttest_p);

%% Get stats (t, p, effect size) for each parameter
current_fieldnames = fieldnames(experiment_results_to_save);
fields_to_remove = {'Code'; 'Group'; 'Age'; 'Gender'; ...
    'index_in_minutes_of_Start_sleep'; 'index_in_minutes_of_Wake_up'; ...
    'Wake_NormalizationRatio'; 'Sleep_NormalizationRatio'};
current_fieldnames = setdiff(current_fieldnames, fields_to_remove);
groups = {experiment_results_to_save.Group};
[unique_groups, ~, groups_mapping] = unique(groups);

ttest_stats = cell(size(current_fieldnames));

for field_index = 1:numel(current_fieldnames)
    current_fieldname = current_fieldnames{field_index};
    groups_values = cell(size(unique_groups));
    for group_index = 1:numel(unique_groups)
        groups_values{group_index} = [experiment_results_to_save(groups_mapping == group_index).(current_fieldname)];
    end
    
    [~, ttest_p, ~, ttest_stats{field_index}] = ttest2(groups_values{:});
    ttest_stats{field_index}.p = ttest_p;
    cohens_d = (mean(groups_values{1}) - mean(groups_values{2}))/ttest_stats{field_index}.sd;
    ttest_stats{field_index}.CohensD = cohens_d;
    ttest_stats{field_index}.Fieldname = current_fieldname;
    
    for group_index = 1:numel(unique_groups)
        ttest_stats{field_index}.(['Mean', unique_groups{group_index}])  = nanmean(groups_values{group_index});
        ttest_stats{field_index}.(['SD', unique_groups{group_index}])  = nanstd(groups_values{group_index});
    end
    ttest_stats{field_index}.AllValues = [groups_values{:}];
end
ttest_stats = [ttest_stats{:}];

%% Take only BM fields
bm_fieldnames = {'Average_Exhale_Duration', ...
    'Average_Exhale_Pause_Duration', ...
    'Average_Exhale_Volume', ...
    'Average_Inhale_Duration', ...
    'Average_Inhale_Pause_Duration', ...
    'Average_Inhale_Volume', ...
    'Average_Inter_Breath_Interval', ...
    'Average_Peak_Expiratory_Flow', ...
    'Average_Peak_Inspiratory_Flow', ...
    'Average_Tidal_Volume', ...
    'Breathing_Rate', ...
    'Coefficient_of_Variation_of_Breath_Volumes', ...
    'Coefficient_of_Variation_of_Breathing_Rate', ...
    'Coefficient_of_Variation_of_Exhale_Duty_Cycle', ...
    'Coefficient_of_Variation_of_Exhale_Pause_Duty_Cycle', ...
    'Coefficient_of_Variation_of_Inhale_Duty_Cycle', ...
    'Coefficient_of_Variation_of_Inhale_Pause_Duty_Cycle', ...
    'Duty_Cycle_of_Exhale', ...
    'Duty_Cycle_of_Exhale_Pause', ...
    'Duty_Cycle_of_Inhale', ...
    'Duty_Cycle_of_Inhale_Pause', ...
    'Minute_Ventilation', ...
    'Percent_of_Breaths_With_Exhale_Pause', ...
    'Percent_of_Breaths_With_Inhale_Pause'};

convert_fieldnames = struct('bm_field', bm_fieldnames, 'our_field', ...
    {'Exhale_Duration', ...
    'Exhale_Pause_Duration', ...
    'Exhale_Volume', ...
    'Inhale_Duration', ...
    'Inhale_Pause_Duration', ...
    'Inhale_Volume', ...
    'Inter_breath_interval', ...
    'Exhale_value', ...
    'Inhale_value', ...
    'Tidal_volume', ...
    'RatePerMinute', ...
    'COV_InhaleVolume', ... % See breathmetrics_master\breathmetrics_functions\getSecondaryRespiratoryFeatures.m, Ln 140
    'COV_RatePerMinute', ...
    'COV_ExhaleDutyCycle', ...
    'COV_ExhalePauseDutyCycle', ...
    'COV_InhaleDutyCycle', ...
    'COV_InhalePauseDutyCycle', ...
    'Duty_Cycle_exhale', ...
    'Duty_Cycle_ExhalePause', ...
    'Duty_Cycle_inhale', ...
    'Duty_Cycle_InhalePause', ...
    'Minute_Ventilation', ...
    'PercentBreathsWithExhalePause', ...
    'PercentBreathsWithInhalePause'});

IPPM_field_str = 'Respiration_peaks_count_avg';
ttest_stats_only_BM_Wake = ttest_stats(ismember({ttest_stats.Fieldname}, strcat('Wake_', {convert_fieldnames.our_field})) | ...
    ismember({ttest_stats.Fieldname}, strcat(IPPM_field_str, '_wake')));
ttest_stats_only_BM_Sleep = ttest_stats(ismember({ttest_stats.Fieldname}, strcat('Sleep_', {convert_fieldnames.our_field})) | ...
    ismember({ttest_stats.Fieldname}, strcat(IPPM_field_str, '_sleep')));

ttest_stats = [ttest_stats_only_BM_Wake, ttest_stats_only_BM_Sleep];
[~, sorting_order] = sort([ttest_stats.p]);
ttest_stats = ttest_stats(sorting_order);
clear sorting_order

%% Get correlation matrix
correlation_matrix = nan(numel(ttest_stats));
corr_th = 0.7;

field_to_skip = false(numel(ttest_stats), 1);
correlated_fields = cell(numel(ttest_stats), 1);
for i=1:numel(ttest_stats)
    for j=1:(i-1)
        correlation_matrix(i, j) = corr(ttest_stats(i).AllValues', ttest_stats(j).AllValues');
    end
    highly_correlated_fields = abs(correlation_matrix(i, :)) >= corr_th;
    if any(highly_correlated_fields)
        field_to_skip(i) = true;
        correlated_fields{i} = struct('Field', {ttest_stats(highly_correlated_fields).Fieldname}, ...
            'Correlations', num2cell(correlation_matrix(i, highly_correlated_fields)));
    end
end
ttest_stats_to_remove = ttest_stats(field_to_skip);
[ttest_stats_to_remove.CorrelatedFields] = deal(correlated_fields{field_to_skip});
ttest_stats_to_keep = ttest_stats(~field_to_skip);
num_of_removed_fields = sum(field_to_skip);

%% Get Supp table
supp_table = cell(2, numel(bm_fieldnames));
for i=1:numel(bm_fieldnames)+1
    if i<=numel(bm_fieldnames)
        [supp_table{:, i}] = deal(struct('Fieldname', convert_fieldnames(i).bm_field));
    else
        [supp_table{:, i}] = deal(struct('Fieldname', 'IPPM'));
    end
    supp_table{1, i}.Phase = 'Wake';
    supp_table{2, i}.Phase = 'Sleep';
    
    for j=1:2
        if i<=numel(bm_fieldnames)
            table_fieldname = [supp_table{j, i}.Phase '_', convert_fieldnames(i).our_field];
        else
            table_fieldname = [IPPM_field_str, '_', lower(supp_table{j, i}.Phase)];
        end
        table_row_boolean = strcmp({ttest_stats.Fieldname}, table_fieldname);
        table_row = ttest_stats(table_row_boolean);
        sds_to_use = [table_row.SDNormosmic, table_row.SDAnosmic];
        log10_of_min_sd = log10(min(sds_to_use));
        precision_required = 10^(floor(log10_of_min_sd)-1);
        normosmics_mean_sd = round([table_row.MeanNormosmic, table_row.SDNormosmic] / precision_required) * precision_required;
        anosmics_mean_sd = round([table_row.MeanAnosmic, table_row.SDAnosmic] / precision_required) * precision_required;
        supp_table{j, i}.Normosmics = [num2str(normosmics_mean_sd(1), 3), '+-', num2str(normosmics_mean_sd(2), 3)];
        supp_table{j, i}.Anosmics = [num2str(anosmics_mean_sd(1), 3), '+-', num2str(anosmics_mean_sd(2), 3)];
        supp_table{j, i}.t = round(table_row.tstat*100)/100;
        supp_table{j, i}.p = round(table_row.p*1000)/1000;
        if isfield(table_row, 'CohensD')
            supp_table{j, i}.CohensD = round(table_row.CohensD*100)/100;
        end
        
        correlated_fields_row = correlated_fields{table_row_boolean};
        if isempty(correlated_fields_row)
            supp_table{j, i}.CorrelatedWith = ' ';
        else
            [~,index] = sortrows([correlated_fields_row.Correlations].');
            correlated_fields_row = correlated_fields_row(index(end:-1:1));
            clear index

            correlated_fieldnames = {correlated_fields_row.Field};
            correlated_fieldnames_clear = strrep(strrep(correlated_fieldnames, 'Sleep_', ''), 'Wake_', '');
            correlated_fieldnames_phases = cellfun(@(str) str(1:find(str=='_', 1)-1), correlated_fieldnames, 'UniformOutput', false); 
            correlated_fieldnames_match = cellfun(@(str) find(strcmp({convert_fieldnames.our_field}, str)), correlated_fieldnames_clear, 'UniformOutput', false);
            correlated_fieldnames_bm = {convert_fieldnames([correlated_fieldnames_match{:}]).bm_field};
            correlated_fieldnames_indices = cellfun(@(str) find(strcmp(bm_fieldnames, str)), correlated_fieldnames_bm, 'UniformOutput', false);
            [correlated_fields_row.Phase] = deal(correlated_fieldnames_phases{:});
            [correlated_fields_row.Index] = deal(correlated_fieldnames_indices{:});
            correlated_fields_strings = arrayfun(@(item) strcat(num2str(item.Index), '-', item.Phase, ' (r=', num2str(item.Correlations, '%1.2f'), ')'), correlated_fields_row, 'UniformOutput', false);
            supp_table{j, i}.CorrelatedWith = strjoin(correlated_fields_strings, '; ');
        end
    end
end
supp_table = [supp_table{:}];
writetable(struct2table(supp_table), 'Supplementary table1.csv');

%% Display all p-values
scatter_size = 40;
ttest_stats_to_display = ttest_stats_to_keep;
figure_str = 'Wake and sleep, without highly-correlated';

p_values = [ttest_stats_to_display.p];
f = figure('Position', [680, 360, 560, 420], 'Color', [1,1,1], 'Name', figure_str);

wake_fields = startsWith({ttest_stats_to_display.Fieldname}, 'Wake_') | endsWith({ttest_stats_to_display.Fieldname}, '_wake');
sleep_fields = startsWith({ttest_stats_to_display.Fieldname}, 'Sleep_') | endsWith({ttest_stats_to_display.Fieldname}, '_sleep');
if any (~wake_fields & ~sleep_fields)
    warning('Field without color!');
end
indices = 1:numel(p_values);
scatter_obj = scatter(indices(wake_fields), p_values(wake_fields), scatter_size, 'o', 'filled', 'MarkerFaceColor', [1, 1, 0] * 0.9);
hold on;
scatter_obj(2) = scatter(indices(sleep_fields), p_values(sleep_fields), scatter_size, 'o', 'filled', 'MarkerFaceColor', [0.5, 0.5, 0.5]*0);
leg_obj = legend(scatter_obj, 'Wake', 'Sleep', 'AutoUpdate', 'off', 'LineWidth', 0.5, 'Location', 'SouthEast');
leg_obj.Position(1:2) = [0.773, 0.22];
leg_obj.Units = 'pixels';
leg_obj.ItemTokenSize(1) = 15;

hold on;
plot(1:numel(p_values), linspace(0.05, 0.05, numel(p_values)), 'r--', 'LineWidth', 2);
xlim([1, numel(p_values)]);
ylabel('p value');
xlabel('Rank order')
f.CurrentAxes.Units = 'pixels';
f.CurrentAxes.Box = 'off';
set(f.CurrentAxes, 'FontSize', 14, 'FontName', 'Calibri', 'FontWeight', 'bold', 'LineWidth', 2);
new_ax_width = min(f.CurrentAxes.Position(3:4));
ax_shrink_size = f.CurrentAxes.Position(3) - new_ax_width;
f.CurrentAxes.Position(3:4) = new_ax_width;
leg_obj.Position(1) = leg_obj.Position(1) - ax_shrink_size;
f.Position(3:4) = [420, 400];

%% Take fields for classifier based on p-value
for option_index = 1:3
    switch option_index
        case 1
            fields_to_take = {ttest_stats([ttest_stats.p]<=0.0074).Fieldname};
            option_str = '_SmallPValues';
        case 2
            fields_to_take = {ttest_stats([ttest_stats.p]<=0.0074 & ~startsWith({ttest_stats.Fieldname}, 'Respiration_peaks_count_avg_')).Fieldname};
            option_str = '_SmallPValuesWithoutIPPM';
        case 3
            fields_to_take = {ttest_stats([ttest_stats.p]<=0.0074 & ~startsWith({ttest_stats.Fieldname}, 'Sleep_')).Fieldname};
            option_str = '_SmallPValuesWithoutSleep';
    end
    
    struct_for_classification = struct('Group', {experiment_results_to_save.Group});
    for i=1:numel(fields_to_take)
        [struct_for_classification.(fields_to_take{i})] = deal(experiment_results_to_save.(fields_to_take{i}));
    end
    table1 = struct2table(struct_for_classification);
    
    % Create permutation for classification
    classifiersType = 'KNN';
    num_of_permutations = 10000;
    toLeaveOneOut = true;
    toLeaveOneOutAndEqualizeGroups = true;
    toPrintResults = true;
    permutations_filename = sprintf('PermutationAccuracyResults_%s_%d_%s.mat', classifiersType, num_of_permutations, option_str);
    
    if exist(permutations_filename, 'file')
        fprintf('Loading permutation results from the file: %s\n', permutations_filename);
        
        loaded_permutations = load(permutations_filename);
        accuracyResultsCat = loaded_permutations.accuracyResultsCat;
        actucalAccuracyResults = loaded_permutations.actucalAccuracyResults;
        validationResults = loaded_permutations.validationResults;
        
        fprintf('Accuracy results for %s classifier: Total=%2.1f%%, TPR=%2.1f%%, TNR=%2.1f%%\n', ...
            classifiersType, actucalAccuracyResults.Total*100, actucalAccuracyResults.TPR*100, actucalAccuracyResults.TNR*100);
    else
        accuracyResults = cell(num_of_permutations, 1);
        start_time = tic;
        [~, ~, validationResults, actucalAccuracyResults] = trainClassifier(table1, classifiersType, toLeaveOneOut, toLeaveOneOutAndEqualizeGroups, toPrintResults);
        fprintf('Accuracy results for %s classifier: Total=%2.1f%%, TPR=%2.1f%%, TNR=%2.1f%%\n', ...
            classifiersType, actucalAccuracyResults.Total*100, actucalAccuracyResults.TPR*100, actucalAccuracyResults.TNR*100);
        toPrintResults = false;
        parfor idx=1:num_of_permutations
            permutations = randperm(height(table1));
            tableValues = table1;
            shuffledGroups = tableValues(permutations, :).Group;
            tableValues.Group = shuffledGroups;
            [~, ~, ~, accuracyResults{idx}] = trainClassifier(tableValues, classifiersType, toLeaveOneOut, toLeaveOneOutAndEqualizeGroups, toPrintResults);
            fprintf('Finished iteration #%d out of %d in %1.3f seconds.\n', idx, num_of_permutations, toc(start_time));
        end
        accuracyResultsCat = [accuracyResults{:}];
        fprintf('Finished all iterations (%d) in %1.3f seconds.\n', num_of_permutations, toc(start_time));
        save(permutations_filename, 'accuracyResultsCat', 'validationResults', 'actucalAccuracyResults');
    end
    
    %% Display permutation results
    f = figure('Color', [1,1,1]);
    valuesToDisplay = [accuracyResultsCat.Total];
    hist_step = min(diff(unique(valuesToDisplay)));
    max_value = max(valuesToDisplay);
    max_value = ceil((max_value+0.01)*20)/20;
    hist_obj = histogram(valuesToDisplay, 0.2:hist_step:max_value);
    hist_obj.FaceColor = [0.3,0.3,0.3];
    hold on;
    plot(actucalAccuracyResults.Total.*[1,1], ylim, 'r--', 'LineWidth', 2);
    p_value = sum(actucalAccuracyResults.Total <=  [actucalAccuracyResults.Total, valuesToDisplay]) ./ (numel(valuesToDisplay)+1);
    fprintf('P-value for actual-value: %1.4f\n', p_value);
    xlabel('Classification accuracy');
    ylabel('Occurrences');
    
    current_ax = f.CurrentAxes;
    set(current_ax, 'FontSize', 14, 'FontName', 'Calibri', ...
        'FontWeight', 'bold', 'LineWidth', 2, 'Box', 'off');
    current_ax.YLabel.Position(1) = -0.09;
    current_ax.XLim = [0, 0.85];
    current_ax.Units = 'pixels';
    current_ax.Position(3:4) = 330;
    f.Position(3:4) = [410, 400];
    
    %% Create ROC figure
    actual_normosmics = strcmp(table1.Group, 'Normosmic');
    actual_anosmics = strcmp(table1.Group, 'Anosmic');
    validationScores = [validationResults.Confidence]';
    validationScores(strcmp({validationResults.Prediction}, 'Normosmic')) = -validationScores(strcmp({validationResults.Prediction}, 'Normosmic'));
    
    f = CreateRocFigure(validationScores, actual_normosmics, actual_anosmics, ...
        table1.Group);
    
    current_ax = f.CurrentAxes;
    set(current_ax, 'FontSize', 14, 'FontName', 'Calibri', ...
        'FontWeight', 'bold', 'LineWidth', 2, 'Box', 'off');
    f.CurrentAxes.Children(2).Color = [0.3,0.3,0.3];
    f.CurrentAxes.Children(2).MarkerFaceColor = [0.3,0.3,0.3];
    f.CurrentAxes.YLabel.Position(1) = 0.11;
    f.CurrentAxes.Units = 'pixels';
    f.CurrentAxes.Position(3:4) = 330;
    f.Position(3:4) = [410, 400];
end
