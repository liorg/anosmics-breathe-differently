%% Load all the data
data_folder = '../Data_Odorant-Free-Room/';
loaded_data = ReadAllSessionsLabcharts(data_folder);
data = [loaded_data.RelevantSessions];
[data.SleepIndex] = deal(nan);
[data.WakeUpIndex] = deal(1);

screen_size = get(0, 'ScreenSize');

%% Display data
number_of_rows = ceil(sqrt(numel(data)));
number_of_cols = ceil(numel(data)/number_of_rows);
figure('Position', screen_size, 'Color', [1,1,1]);
for i=1:numel(data)
    subplot(number_of_rows, number_of_cols, i);
    time_vector = data(i).Time;
    summed_data = data(i).Respiration;
    plot(time_vector, summed_data);
    title(data(i).SubjectName);
    xlim([500, 620]);
    grid on;
end

%% For each participant, find both IPPM and BPM
lpf_freq = 6; % Hz, to match the data from the main experiment
fprintf('Downsampling to %1.2f Hz.\n', lpf_freq);
block_length_in_minutes = 5;
analysis_results = cell(size(data));
start_time = tic;
figure('Position', screen_size, 'Color', [1,1,1], 'Name', sprintf('%d Hz', lpf_freq));
for i=1:numel(data)
    sample_length = 1/data(i).Tickrate;
    
    current_freq = 1/sample_length;
    old_data = cat(2, data(i).Respiration, data(i).Respiration2Channels);
    old_data(isnan(old_data)) = 0;
    old_timepoints = (1:size(old_data, 1))-1;
    new_timepoints = old_timepoints*current_freq/lpf_freq;
    new_timepoints(new_timepoints>old_timepoints(end)) = [];
    new_data = interp1(old_timepoints, old_data, new_timepoints,'spline');
    
    current_participant = data(i);
    current_participant.SampleLength = 1/lpf_freq;
    current_participant.Data = new_data(:, 1);
    current_participant.Respiration2Channels = new_data(:, 2:3);
    current_participant = rmfield(current_participant, {'Time', 'Respiration'});

    respiratoryDataSplitted = GetDataSplitted(current_participant.Data, current_participant.SleepIndex, current_participant.WakeUpIndex, current_participant.SampleLength, block_length_in_minutes);
    respiratoryDataSplitted(strcmp({respiratoryDataSplitted.Arousal}, 'Sleep')) = [];
    respiratoryFeatures = RespiratoryFeaturesForSplittedData(respiratoryDataSplitted);
    respiratoryFeaturesMeans = MeanRespiratoryFeatureByArousalState(respiratoryFeatures);
    
    respiratoryPeaks = GetPeaksData(current_participant.Data, current_participant.SleepIndex, current_participant.WakeUpIndex, current_participant.SampleLength);
    
    block_length_in_minutes = 5;
    WAKE_SLEEP_ENUM = struct('WAKE_ONLY', 0, 'SLEEP_ONLY', 1, 'WAKE_AND_SLEEP', 2);
    wake_sleep_options = [WAKE_SLEEP_ENUM.WAKE_ONLY, WAKE_SLEEP_ENUM.SLEEP_ONLY];
    max_peaks_in_minute = 40;
    min_peaks_in_minute = 5;
    peaksAnalysisPerSubjectAll = AnalysisPeaksPerSubject(respiratoryPeaks, ...
        block_length_in_minutes, min_peaks_in_minute, max_peaks_in_minute, ...
        WAKE_SLEEP_ENUM, wake_sleep_options);
    
    %% Store everything
    analysis_results{i}.RespiratoryFeatures = respiratoryFeatures;
    analysis_results{i}.RespiratoryFeaturesMeans = respiratoryFeaturesMeans;
    analysis_results{i}.RespiratoryPeaks = respiratoryPeaks;
    analysis_results{i}.RespiratoryPeaksFeatures = [peaksAnalysisPerSubjectAll{:}];
    
    %% Display two-types of peaks
    subplot(number_of_rows, number_of_cols, i);
    time_vector = (0:numel(current_participant.Data)-1)*current_participant.SampleLength;
    plot(time_vector, current_participant.Data);
    hold on;
    scatter(respiratoryPeaks.InhalePeaksTimes, respiratoryPeaks.InhalePeaks/respiratoryPeaks.InputDataRatio, 100, 'r');
    for j=1:numel(respiratoryFeatures.Peaks)
        currentPeaksToDisplay = respiratoryFeatures.Peaks{j};
        peaksTimes = [currentPeaksToDisplay.StartTime] + [currentPeaksToDisplay.Latency] + (j-1)*block_length_in_minutes*60;
        peaksHeights = [currentPeaksToDisplay.PeakValue] / respiratoryDataSplitted.NormalizationRatio;
        peaksToDisplay = peaksHeights > 0;
        scatter(peaksTimes(peaksToDisplay), peaksHeights(peaksToDisplay), 50, 'b','filled', 'MarkerFaceAlpha', 0.3);
    end
    title(current_participant.SubjectName);
    xlim([200, 230]);
    grid on;
end

%% Merge everything
analysis_results_concatenated = [analysis_results{:}];
removed_participants = cellfun(@isempty, analysis_results);
experiment_results = rmfield(data(~removed_participants), {'Tickrate', 'SleepIndex', 'WakeUpIndex', 'Time', 'Respiration', 'Respiration2ChannelsOriginal', 'Respiration2Channels'});
[experiment_results.Code] = deal(experiment_results.SubjectName);
[experiment_results.Group] = deal('OdorantFree');

% Merge Respiratory-features
respiratory_features_concatenated = [analysis_results_concatenated.RespiratoryFeaturesMeans];
respiratory_features_fields = fieldnames(respiratory_features_concatenated);
for respiratory_features_field_index = 1:numel(respiratory_features_fields)
    current_field_name = respiratory_features_fields{respiratory_features_field_index};
    current_field_values = {respiratory_features_concatenated.(current_field_name)};
    if all(cellfun(@(values) isnumeric(values) && numel(values) == 1, current_field_values))
        [experiment_results.(current_field_name)] = deal(current_field_values{:});
    end
end

% Merge respiratory-peaks
respiratory_peaks_concatenated = cat(1, analysis_results_concatenated.RespiratoryPeaksFeatures);
inhalePeaksCountAvg = arrayfun(@(item) nanmean(item.InhalePeaksCount)/block_length_in_minutes, respiratory_peaks_concatenated, 'UniformOutput', false);

[experiment_results.Respiration_peaks_count_avg_wake] = deal(inhalePeaksCountAvg{:, wake_sleep_options == WAKE_SLEEP_ENUM.WAKE_ONLY});

[experiment_results.IPPM] = deal(experiment_results.Respiration_peaks_count_avg_wake);
[experiment_results.BPM] = deal(experiment_results.Wake_RatePerMinute);

%% Remove outliers
fields_for_outliers = {'IPPM', 'BPM'};
outliers_indices = false(size(experiment_results));
for i=1:numel(fields_for_outliers)
    current_field = fields_for_outliers{i};
    current_values = [experiment_results.(current_field)];
    quartiles = prctile(current_values, [25, 50, 75]);
    iqr_width = diff(quartiles([1,3]));
    outliers_thresholds = quartiles([1,3]) + [-1, 1] * 1.5 * iqr_width;
    current_outliers = current_values < outliers_thresholds(1) | current_values > outliers_thresholds(2);
    outliers_indices(current_outliers) = true;

    fprintf('For field %s, found %d outliers. Outliers thresholds: [%1.2f, %1.2f]', ...
        current_field, sum(current_outliers), outliers_thresholds);
    current_outliers_indices = find(current_outliers);
    for ind=1:numel(current_outliers_indices)
        current_ind = current_outliers_indices(ind);
        fprintf(', %s (%1.2f)', experiment_results(current_ind).Code, current_values(current_ind));
    end
    fprintf('\n');
end
experiment_results(outliers_indices) = [];

%% Display results

fields_to_display = {'BPM', 'IPPM'};
fields_prefix = '';
groups_colors = {[0.5, 0.5, 0.5]};
groups_titles = {'OdorantFree'};

sleep_vs_wake_scatter_figure = DisplayXYScatterPerGroup(experiment_results, fields_to_display, fields_prefix, groups_titles, groups_colors);

%% Display raincloud figures
sleep_vs_wake_scatter_figure_to_be_merged = DisplayXYScatterPerGroup(experiment_results, fields_to_display, fields_prefix, groups_titles, groups_colors);
bars_x_ranges = {[-0.029, -0.012], [-0.05, -0.033]};
raincloud_figures = DisplayRainclouds(experiment_results, ...
    {fields_to_display}, groups_titles, groups_colors, bars_x_ranges);

% Merge raincloud and y-x figures
raincloud_panel_width = 75;
merged_figures = MergeFigures(sleep_vs_wake_scatter_figure_to_be_merged, raincloud_figures, raincloud_panel_width);
close([sleep_vs_wake_scatter_figure_to_be_merged, raincloud_figures]);

%% Write results to CSV
experiment_results_to_save = experiment_results;
% Remove irrelevant fields
structs_fieldnames = fieldnames(experiment_results_to_save);
fields_to_display_flat = [fields_to_display{:}]';
for idx=1:numel(structs_fieldnames)
    current_fieldname = structs_fieldnames{idx};
    if ismember(current_fieldname, fields_to_display_flat)
        continue;
    end
    if ismember(current_fieldname, {'SubjectName', 'Code', 'Gender', 'Group'})
        continue;
    end
    current_field_values = {experiment_results_to_save.(current_fieldname)};
    if ischar(current_field_values{1})
        unique_values = unique(current_field_values);
        if numel(unique_values) == 1
            fprintf('Removing field with single value: %s\n', current_fieldname)
            experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
            continue;
        end
        fprintf('Removing field with strings: %s\n', current_fieldname)
        experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
        continue;
    end
    numel_per_field = cellfun(@numel, current_field_values);
    if any(numel_per_field > 1)
        fprintf('Removing field with vector(s): %s\n', current_fieldname)
        experiment_results_to_save = rmfield(experiment_results_to_save, current_fieldname);
    end
end

group_str = experiment_results(1).Group;
participants = strcmp({experiment_results_to_save.Group}, group_str);
file_postfix = ['_', group_str];
hz_postfix = sprintf('_%d Hz', lpf_freq);
filename_for_results = ['Respiration_All_Results', file_postfix, hz_postfix, '.csv'];
writetable(struct2table(experiment_results_to_save(participants)), filename_for_results);

%% Compare normosmics in the small experiment to anosmics in main experiment %%
%% Load data
if exist('experiment_results_to_save', 'var')
    odorant_free_experiment_data = experiment_results_to_save;
else
    filename_for_results = 'Respiration_All_Results_OdorantFree_6 Hz.csv';
    odorant_free_experiment_data = table2struct(readtable(filename_for_results));
end

real_experiment_data = table2struct(readtable('Respiration_All_Results.csv'));
if ~isfield(real_experiment_data, 'Type')
    [real_experiment_data.Type] = deal(real_experiment_data.Group);
end

real_experiment_data_anosmics = real_experiment_data(strcmp({real_experiment_data.Type}, 'Anosmic'));

%% Print means and stats for IPPM
odorant_free_experiment_IPPMs = [odorant_free_experiment_data.IPPM];
real_experiment_anosmics_IPPMs = [real_experiment_data_anosmics.Respiration_peaks_count_avg_wake];

data_stats = @(IPPMs, ratios) [numel(IPPMs), nanmean(IPPMs), nanstd(IPPMs), nanmedian(IPPMs)];
data_string = 'IPPM = %1.1f +- %1.1f (Median=%1.1f)';
fprintf(['Odorant free normosmics (N=%d): ', data_string, '\n'], ...
    data_stats(odorant_free_experiment_IPPMs));
fprintf(['Main exp. anosmics (N=%d): ', data_string, '\n'], ...
    data_stats(real_experiment_anosmics_IPPMs));

print_ttest2_info('IPPM of odorant-free normosmia vs. anosmia', odorant_free_experiment_IPPMs, real_experiment_anosmics_IPPMs);

%% Aux function %%

function print_ttest2_info(header_text, group1, group2)

[~, ttest_p, ~, ttest_stats] = ttest2(group1, group2);
fprintf('Comparing %s: t(%d)=%1.2f, p=%1.3f.\n', ...
    header_text, ...
    ttest_stats.df, ttest_stats.tstat, ttest_p);

end