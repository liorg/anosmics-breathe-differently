function raincloud_figures = DisplayRainclouds(experiment_results, ...
    fields_to_display, groups_titles, groups_colors, bars_x_ranges)

%% Define constants
max_value_for_density = 0.075;

%% Map participant to group
grouping_indices = cellfun(@(group_title) strcmp({experiment_results.Group}, group_title), groups_titles, 'UniformOutput', false);

%% Create figure for each field (or sub-list of fields)
raincloud_figures = cell(size(fields_to_display));
for field_index = 1:numel(fields_to_display)
    f = nestedDisplayRaincloudsForField(experiment_results, fields_to_display{field_index}, ...
        grouping_indices, groups_colors, bars_x_ranges, max_value_for_density);

    raincloud_figures{field_index} = f;
end
raincloud_figures = [raincloud_figures{:}];

%% Set figure properties for all figures
set([raincloud_figures.Children], 'FontSize', 14, 'FontName', 'Calibri', 'FontWeight', 'bold', 'YTick', 0:5:40);

end


function f = nestedDisplayRaincloudsForField(experiment_results, current_fields_to_display, ...
    grouping_indices, groups_colors, bars_x_ranges, max_value_for_density)

f = figure('Color', [1,1,1], 'Name', ['Raincloud for ', current_fields_to_display{1}]);
subplot_index = 0;
for sleep_or_wake = 0:1
    %% Display raincloud for current phase
    subplot_index = subplot_index + 1;
    subplot(1,2,subplot_index);
    if sleep_or_wake == 0
        fieldname = current_fields_to_display{1};
    else
        fieldname = current_fields_to_display{2};
    end
    
    current_field_values = [experiment_results.(fieldname)];
    nestedDisplayRaincloudsForPhase(current_field_values, ...
        grouping_indices, groups_colors, bars_x_ranges);
    
    %% Set order of raincloud objects
    ax = gca;
    raincloud_plot_objects = ax.Children;
    raincloud_plot_objects_types = arrayfun(@(item) item.Type, raincloud_plot_objects, 'UniformOutput', false);
    
    % Set scatter-objects to be on the bottom
    scatter_objects = raincloud_plot_objects(strcmp(raincloud_plot_objects_types, 'scatter'));
    uistack(scatter_objects, 'bottom');
    
    % Set area-objects width
    area_objects = raincloud_plot_objects(strcmp(raincloud_plot_objects_types, 'patch'));
    max_area_value = max(cat(1, area_objects.XData));
    area_object_scaling = max_value_for_density / max_area_value;
    for area_object_index = 1:numel(area_objects)
        area_objects(area_object_index).XData = area_objects(area_object_index).XData * area_object_scaling;
    end
    
    %% Set limits of the panel
    ylim('auto');
    xlim([-0.055, 0.08]);
    ax.Units = 'points';
    ax.Position(3) = 120;
    
    if subplot_index ~= 1
        ax.YTickLabel = {};
    end
    
end
all_axes = [f.Children];
linkaxes(all_axes, 'y');

end


function nestedDisplayRaincloudsForPhase(current_field_values, ...
    grouping_indices, groups_colors, bars_x_ranges)

for group = 1:numel(groups_colors)
    bar_x_range = bars_x_ranges{group};
    subjects_to_display = grouping_indices{group};
    values_to_display = current_field_values(subjects_to_display);
    
    %% Display typical raincloud
    rng(5287);
    raincloud_plot_objects = raincloud_plot(values_to_display, ...
        groups_colors{group}, true);
    
    %% Split raincloud to objects
    raincloud_plot_objects_types = cellfun(@(item) item.Type, raincloud_plot_objects, 'UniformOutput', false);
    area_object = raincloud_plot_objects{strcmp(raincloud_plot_objects_types, 'patch')};
    rectangle_object = raincloud_plot_objects{strcmp(raincloud_plot_objects_types, 'rectangle')};
    lines_objects = [raincloud_plot_objects{strcmp(raincloud_plot_objects_types, 'line')}];
    scatter_object = raincloud_plot_objects{strcmp(raincloud_plot_objects_types, 'scatter')};
    
    %% Get info about the raincloud
    lines_y_values = [lines_objects.YData];
    lines_width = max(lines_y_values)-min(lines_y_values);
    highest_lines_y_value = max(lines_y_values) + 0.1*lines_width;
    
    %% Re-define parameters for the different objects
    set(area_object, 'LineStyle', 'none', 'FaceAlpha', 0.75);
    
    set(scatter_object, 'SizeData', 25, 'LineWidth', 0.1, ...
        'MarkerEdgeColor', [0,0,0], 'MarkerEdgeAlpha', 0.5);
    
    set(lines_objects, 'LineWidth', 2);
    set(rectangle_object, 'LineWidth', 2);
        
    box_plot_color = groups_colors{group} * 0.6;
    [rectangle_object.EdgeColor, lines_objects.Color] = deal(box_plot_color);
    
    %% Redefine positions for different objects
    rectangle_object.Position(2) = rectangle_object.Position(2) - highest_lines_y_value;
    boxplot_center = rectangle_object.Position(2)+rectangle_object.Position(4)/2;
    offset_by_group = -boxplot_center*0.5 + boxplot_center * group;
    
    rectangle_object.Position([2,4]) = rectangle_object.Position([2,4])*0.5 + offset_by_group * [1,0];
    for i=1:numel(lines_objects)
        lines_objects(i).YData = lines_objects(i).YData - highest_lines_y_value;
        lines_objects(i).YData = lines_objects(i).YData*0.5 + offset_by_group;
    end
    scatter_object.YData = scatter_object.YData - highest_lines_y_value;
    scatter_object.YData = scatter_object.YData*0.5 + offset_by_group;
    
    %% Rotate
    for i=1:numel(raincloud_plot_objects)
        if strcmp(raincloud_plot_objects_types{i}, 'rectangle')
            raincloud_plot_objects{i}.Position = raincloud_plot_objects{i}.Position([2,1,4,3]);
        else
            set(raincloud_plot_objects{i}, 'XData', raincloud_plot_objects{i}.YData, ...
                'YData', raincloud_plot_objects{i}.XData);
        end
    end
    
    %% Shrink the bar and rain-cloud to the required width
    rectangle_object.Position([1,3]) = [bar_x_range(1), diff(bar_x_range)];
    vertical_lines = arrayfun(@(line_obj) diff(line_obj.XData) == 0, lines_objects);
    [lines_objects(vertical_lines).XData] = deal([1,1]*mean(bar_x_range));
    [lines_objects(~vertical_lines).XData] = deal(bar_x_range);
    scatter_object.XData = (scatter_object.XData - min(scatter_object.XData)) / (max(scatter_object.XData)-min(scatter_object.XData)) * diff(bar_x_range) + bar_x_range(1);
    
    %% Hold on, for the next raincloud
    hold on;
end
end

