function respiratoryFeaturesMeans = MeanRespiratoryFeatureByArousalState(respiratoryFeatures)

%% Flat respiratory-features
respiratoryFeaturesFields = fieldnames(respiratoryFeatures);
respiratoryFeaturesFlat = cell(size(respiratoryFeatures));
for idx=1:numel(respiratoryFeatures)
    currentRespiratoryFeaturesData = respiratoryFeatures(idx).Data;
    if isempty(currentRespiratoryFeaturesData)
        continue;
    end
    respiratoryFeaturesFlat{idx} = repmat(struct(), size(currentRespiratoryFeaturesData));
    for j=1:numel(respiratoryFeaturesFields)
        current_field = respiratoryFeaturesFields{j};
        if ~strcmp(current_field, 'Data')
            [respiratoryFeaturesFlat{idx}.(current_field)] = deal(respiratoryFeatures(idx).(current_field));
        end
    end
    dataFields = fieldnames(currentRespiratoryFeaturesData);
    for j=1:numel(dataFields)
        current_field = dataFields{j};
        [respiratoryFeaturesFlat{idx}.(current_field)] = deal(currentRespiratoryFeaturesData.(current_field));
    end
end
respiratoryFeaturesFlat = [respiratoryFeaturesFlat{:}];
respiratoryFeaturesFlatToExclude = [respiratoryFeaturesFlat.Inhale_Count] > 5*40 | [respiratoryFeaturesFlat.Inhale_Count] < 5*5 | ...
    [respiratoryFeaturesFlat.Exhale_Count] > 40*5 | [respiratoryFeaturesFlat.Exhale_Count] < 5*5;
respiratoryFeaturesFlatClear = respiratoryFeaturesFlat(~respiratoryFeaturesFlatToExclude);

%% Get mean per property per Arousal State
respiratoryFeaturesFlatClearFields = fieldnames(respiratoryFeaturesFlatClear);
arousalStates = unique({respiratoryFeaturesFlatClear.Arousal});

respiratoryFeaturesMeans = struct();
for arousalIndex = 1:numel(arousalStates)
    arousal_state = arousalStates{arousalIndex};
    currentArousalAndParticipant = respiratoryFeaturesFlatClear(strcmp({respiratoryFeaturesFlatClear.Arousal}, arousal_state));
    
    for fieldIndex = 1:numel(respiratoryFeaturesFlatClearFields)
        current_fieldname = respiratoryFeaturesFlatClearFields{fieldIndex};
        if ismember(current_fieldname, {'Name', 'Group', 'Arousal', 'SampleLength', 'SamplingRate', 'SamplesInEachBlock', 'Peaks'})
            continue;
        end
        if ~isnumeric(currentArousalAndParticipant(1).(current_fieldname))
            warning('Found non-numeric field: %s', current_fieldname);
            continue;
        end
        respiratoryFeaturesMeans.([arousal_state, '_', current_fieldname]) = nanmean([currentArousalAndParticipant.(current_fieldname)]);
    end
end
