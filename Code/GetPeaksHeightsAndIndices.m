function [peaks_heights, peaks_indices] = GetPeaksHeightsAndIndices(data, peaks_properties, peak_min_height)

[peaks_heights, indices] = findpeaks(data, peaks_properties{:});

peaks_to_keep = peaks_heights > peak_min_height;

peaks_heights = peaks_heights(peaks_to_keep);
peaks_indices = indices(peaks_to_keep);
