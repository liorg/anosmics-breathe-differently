function peaksData = GetPeaksData(InputData, sleep_time_index, wake_up_index, sampleLength)

%% Get vector for both nostrils together
if size(InputData, 2) == 2
    Data_TwoNostrils = InputData;
    max_to_min_input_data = max(Data_TwoNostrils, [], 1) - min(Data_TwoNostrils, [], 1);
    input_data_ratio_multiplier = 5 ./ max_to_min_input_data;
    Data_TwoNostrils_Normalized = Data_TwoNostrils .* repmat(input_data_ratio_multiplier, [size(Data_TwoNostrils, 1), 1]);
    Data = sum(Data_TwoNostrils_Normalized, 2);
else
    max_to_min_input_data = max(InputData, [], 1) - min(InputData, [], 1);
    input_data_ratio_multiplier = 5 / max_to_min_input_data;
    Data = InputData * input_data_ratio_multiplier;
end

%% Get time variables
time_in_serial_sec_from_start = (0:size(Data, 1)-1)' * sampleLength;
wake_up_time_seconds = wake_up_index * sampleLength;
sleep_time_seconds = sleep_time_index * sampleLength;

%% Auto-correlation of whole data, wake-only and sleep-only
max_lag_xcorr_in_seconds = 10;
[whole_day_period_in_seconds, wake_part_period_in_seconds, sleep_part_period_in_seconds] = nested_CalculatePeriods(Data, ...
    max_lag_xcorr_in_seconds, sampleLength, ...
    wake_up_time_seconds, sleep_time_seconds, time_in_serial_sec_from_start);

numberOfSamples = numel(Data);

%% Find maximums and minimums
peaks_properties = {...
    'MinPeakDistance', 0.5 * whole_day_period_in_seconds, ...
    'MinPeakProminence', 0.1};
peak_min_height = 0.1;
[inhale_peaks, inhale_indices_to_keep] = GetPeaksHeightsAndIndices(Data, peaks_properties, peak_min_height);
[exhale_peaks, exhale_indices_to_keep] = GetPeaksHeightsAndIndices(-Data, peaks_properties, peak_min_height);
exhale_peaks = -exhale_peaks;

inhale_locations = time_in_serial_sec_from_start(inhale_indices_to_keep);
exhale_locations = time_in_serial_sec_from_start(exhale_indices_to_keep);

%% Save peaks
peaksData = struct();
peaksData.InhalePeaks = inhale_peaks;
peaksData.InhalePeaksTimes = inhale_locations;
peaksData.ExhalePeaks = exhale_peaks;
peaksData.ExhalePeaksTimes = exhale_locations;
peaksData.SampleLength = sampleLength;
peaksData.NumberOfSamples = numberOfSamples;
peaksData.WakeUpSeconds = wake_up_time_seconds;
peaksData.SleepTimeSeconds = sleep_time_seconds;
peaksData.WholeDayPeriodInSeconds = whole_day_period_in_seconds;
peaksData.OnlyWakePeriodInSeconds = wake_part_period_in_seconds;
peaksData.OnlySleepPeriodInSeconds = sleep_part_period_in_seconds;
peaksData.InputDataRatio = input_data_ratio_multiplier;

end


function [latency_from_xcorr, peaks_values, peaks_locations] = nested_getDataFromXcorr(xcorr_xvalues, xcorr_result)
[peaks_values, peaks_locations] = findpeaks(xcorr_result, xcorr_xvalues);
[~, peaks_location_abs_order] = sort(abs(peaks_locations));
if numel(peaks_location_abs_order) == 0
    peaks_locations = [];
    peaks_values = [];
    latency_from_xcorr = nan;
    return;
end
if numel(peaks_location_abs_order) == 1
    latency_from_xcorr = nan;
    return;
end
peaks_values = peaks_values(peaks_location_abs_order(1:3));
peaks_locations = peaks_locations(peaks_location_abs_order(1:3));

latency_from_xcorr = unique(abs(peaks_locations(peaks_locations ~= 0)));
end

function [whole_day_period_in_seconds, wake_part_period_in_seconds, sleep_part_period_in_seconds] = nested_CalculatePeriods(Data, ...
    max_lag_xcorr_in_seconds, sampleLength, ...
    wake_up_time_seconds, sleep_time_seconds, time_in_serial_sec_from_start)
    
max_lag_xcorr_in_samples = ceil(max_lag_xcorr_in_seconds / sampleLength);
xcorr_xvalues = (-max_lag_xcorr_in_samples:max_lag_xcorr_in_samples)*sampleLength;

data_sum_scaled = (Data - nanmean(Data)) / nanstd(Data);
data_sum_scaled(isnan(Data)) = 0;

if isnan(sleep_time_seconds)
    wake_indices = true(size(data_sum_scaled));
elseif wake_up_time_seconds >= sleep_time_seconds
    wake_indices = time_in_serial_sec_from_start < sleep_time_seconds | time_in_serial_sec_from_start > wake_up_time_seconds;
else
    wake_indices = time_in_serial_sec_from_start < sleep_time_seconds & time_in_serial_sec_from_start > wake_up_time_seconds;
end
data_sum_scaled_xcorr_whole_day = xcorr(data_sum_scaled, max_lag_xcorr_in_samples);
data_sum_scaled_xcorr_wake_only = xcorr(data_sum_scaled(wake_indices), max_lag_xcorr_in_samples);
data_sum_scaled_xcorr_sleep_only = xcorr(data_sum_scaled(~wake_indices), max_lag_xcorr_in_samples);

[whole_day_period_in_seconds, ~, ~] = nested_getDataFromXcorr(xcorr_xvalues, data_sum_scaled_xcorr_whole_day);
if numel(whole_day_period_in_seconds) ~= 1
    fprintf('Found more than one unique value for latency: %s\n', sprintf('%1.3f, ', whole_day_period_in_seconds));
    whole_day_period_in_seconds = whole_day_period_in_seconds(1);
end

[wake_part_period_in_seconds, ~, ~] = nested_getDataFromXcorr(xcorr_xvalues, data_sum_scaled_xcorr_wake_only);
if numel(wake_part_period_in_seconds) ~= 1
    fprintf('For subject %s, found more than one unique value for latency in wake only: %s\n', subjectName, sprintf('%1.3f, ', wake_part_period_in_seconds));
    wake_part_period_in_seconds = wake_part_period_in_seconds(1);
end

if isempty(data_sum_scaled_xcorr_sleep_only)
    sleep_part_period_in_seconds = nan;
else
    [sleep_part_period_in_seconds, ~, ~] = nested_getDataFromXcorr(xcorr_xvalues, data_sum_scaled_xcorr_sleep_only);
    if numel(sleep_part_period_in_seconds) ~= 1
        fprintf('For subject %s, found more than one unique value for latency in sleep only: %s\n', subjectName, sprintf('%1.3f, ', sleep_part_period_in_seconds));
        if numel(sleep_part_period_in_seconds) > 1
            sleep_part_period_in_seconds = sleep_part_period_in_seconds(1);
        else
            sleep_part_period_in_seconds = nan;
        end
    end
end

end

