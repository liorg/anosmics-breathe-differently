function DisplayMovingMean(BPM_and_IPPM_per_block, fields_to_display, ...
    groups_colors, minimal_participans_required, block_length_in_minutes, displaying_version, ...
    given_ylim)

%% Default argin
if ~exist('displaying_version', 'var')
    displaying_version = 1;
end

if ~exist('given_ylim', 'var')
    given_ylim = [];
end

%% Extract grouping info
group_per_participant = {BPM_and_IPPM_per_block.Group}';
unique_groups = unique(group_per_participant);
unique_groups = unique_groups(end:-1:1);
number_of_unique_groups = numel(unique_groups);

%% Define constants
number_of_blocks_for_moving_median = round(60/block_length_in_minutes + 1);
number_of_fields_to_display = numel(fields_to_display);

%% Define figure parameters
screen_data = get(0);
fig_size = screen_data.ScreenSize;
fig_name = sprintf('Moving mean of %d blocks', number_of_blocks_for_moving_median);
if displaying_version == 2
    fig_size([2, 4]) = fig_size(4)/2 * [0.5,1];
end
figure('Color', [1,1,1], 'Position', fig_size, 'Name', fig_name);

p_line_color = [1,1,1]*0.5;

if displaying_version == 1
    number_of_rows = 2;
    number_of_cols = ceil(number_of_fields_to_display / number_of_rows);
else
    number_of_rows = 1;
    number_of_cols = ceil(number_of_fields_to_display / number_of_rows / 2);
end

%% Display each required field
for field_index = 1:number_of_fields_to_display
    current_field = fields_to_display{field_index};
    if displaying_version == 1
        subplot_index = field_index;
        xaxis_start_offset = 0;
    else
        subplot_index = ceil(field_index/2);
        if mod(field_index, 2) == 1
            xaxis_start_offset = 0;
            bg_color = [1,1,1];
            bg_width = 20;
        else
            xaxis_start_offset = 20;
            bg_color = [1,1,1]*0.9;
            bg_width = 10;
        end
    end
    ax = subplot(number_of_rows, number_of_cols, subplot_index);
    yyaxis('left');
    ax.YColor = [0,0,0];
    values_for_ttest = cell(1, number_of_unique_groups);
    lines_for_legend = cell(1, number_of_unique_groups);
    for group_index = 1:number_of_unique_groups
        current_participants = strcmp(group_per_participant, unique_groups{group_index});
        current_field_values = {BPM_and_IPPM_per_block(current_participants).(current_field)}';
        [lines_for_legend{group_index}, values_for_ttest{group_index}] = nestedDisplayCurrentValues(current_field_values, ...
            xaxis_start_offset, block_length_in_minutes, minimal_participans_required, number_of_blocks_for_moving_median, ...
            groups_colors{group_index}, unique_groups{group_index});
    end
    if displaying_version == 1
        ylabel_str = strrep(current_field, '_', ' ');
    else
        ylabel_str = current_field(1:find(current_field == '_', 1)-1);
    end
    ylabel(ylabel_str);
    xlabel('Time (Hours)');
    if displaying_version == 1
        legend([lines_for_legend{:}], unique_groups, 'Location', 'Best', 'AutoUpdate', 'off', 'Box', 'off');
    else
        patch(xaxis_start_offset + [0, 0, 1, 1]*bg_width, ...
            [0, 1, 1, 0]*50, ...
            bg_color, ...
            'LineStyle', 'none');
        ax = gca;
        ax.Children = ax.Children([2:end, 1]);
    end
    
    % Display p-values line
    if number_of_unique_groups > 1
        [hours_from_start, p_per_block] = nestedTimewiseComparison(values_for_ttest, block_length_in_minutes, minimal_participans_required);
        yyaxis('right');
        plot(xaxis_start_offset + hours_from_start, p_per_block, '--', 'Color', p_line_color, 'LineWidth', 1);
        hold on;
        plot(xlim, 0.05*[1,1], 'k--', 'LineWidth', 0.8);
        ylim([0, 0.2]);
        ylabel('p value');
        ax.YColor = p_line_color;
    end
    
    % Define ax properties
    ax.Box = 'off';
    ax.LineWidth = 2;
    set(ax, 'FontSize', 14, 'FontName', 'Calibri', 'FontWeight', 'bold');
    
    if displaying_version == 2 && mod(field_index, 2) == 0
        ticks_awake = 0:3:15;
        ticks_sleep = 20:3:30;
        ticks_wake_labels = arrayfun(@num2str, ticks_awake, 'UniformOutput', false);
        ticks_sleep_labels = arrayfun(@num2str, ticks_sleep - ticks_sleep(1), 'UniformOutput', false);
        ticks_wake_labels{1} = 'Awake';
        ticks_sleep_labels{1} = 'Sleep';
        ticks = [ticks_awake, ticks_sleep];
        ticks_labels = [ticks_wake_labels, ticks_sleep_labels];
        set(ax, 'XTick', ticks, 'XTickLabel', ticks_labels, 'XLim', [0, max(ticks_sleep)]);
        
        legend([lines_for_legend{:}], unique_groups, 'Location', 'South', 'AutoUpdate', 'off', 'Box', 'off');
        if ~isempty(given_ylim)
            yyaxis('left');
            ylim(ax, given_ylim);
        end
    end
    
    % Print significancy vs. time information
    if number_of_unique_groups > 1
        nestedPrintSignifcancyInfo(p_per_block, hours_from_start, current_field);
    end
end
%% Narrow the side margins
pause(0.05); % Pause for few ms, so the figure will be refreshed
if displaying_version == 2
    f = gcf;
    all_f_children = [f.Children];
    all_f_children_types = arrayfun(@(t) t.Type, all_f_children, 'UniformOutput', false);
    all_ax = all_f_children(strcmp(all_f_children_types, 'axes'));
    set(all_ax, 'Units', 'pixels');
    current_positions = {all_ax.Position};
    min_x_pos = min(cellfun(@(ax_pos) ax_pos(1), current_positions));
    new_min_x_pos = 70;
    new_positions = cellfun(@(ax_pos) ax_pos - (min_x_pos-new_min_x_pos)*[1,0,0,0], current_positions, 'UniformOutput', false);
    [all_ax.Position] = deal(new_positions{:});
    max_x_pos = max(cellfun(@(ax_pos) sum(ax_pos([1, 3])), new_positions));
    new_max_x_pos = 85 + max_x_pos;
    f.Position(1) = (f.Position(3)-new_max_x_pos)/2;
    f.Position(3) = new_max_x_pos;
end

end


function [line_for_legend, values_for_ttest] = nestedDisplayCurrentValues(current_field_values, ...
    xaxis_start_offset, block_length_in_minutes, minimal_participans_required, number_of_blocks_for_moving_median, ...
    group_color, group_name)

number_of_blocks_per_participant = cellfun(@numel, current_field_values);
longest_vector = max(number_of_blocks_per_participant);
current_field_values_matrix = nan(numel(number_of_blocks_per_participant), longest_vector);
for participant = 1:numel(number_of_blocks_per_participant)
    current_field_values_matrix(participant, 1:number_of_blocks_per_participant(participant)) = current_field_values{participant};
end
hours_from_start = (1:longest_vector) * block_length_in_minutes/60;

current_field_values_matrix = movmean(current_field_values_matrix', number_of_blocks_for_moving_median, 'omitnan')';

participant_per_block = sum(~isnan(current_field_values_matrix), 1);
last_block_with_minimal_participants = find(participant_per_block >= minimal_participans_required, 1, 'last');
current_field_values_avg_in_block = nanmean(current_field_values_matrix, 1);
current_field_values_std_in_block = nanstd(current_field_values_matrix, [], 1);

xVector = hours_from_start(1:last_block_with_minimal_participants);
meanVector = current_field_values_avg_in_block(1:last_block_with_minimal_participants);
steVector = current_field_values_std_in_block(1:last_block_with_minimal_participants) ./ sqrt(participant_per_block(1:last_block_with_minimal_participants));
shaedErrorBadObj = shadedErrorBar(xaxis_start_offset + xVector, meanVector, steVector,{'-', 'Color', group_color, 'LineWidth', 2, 'Tag', group_name}, 0.5);
hold on;

line_for_legend = shaedErrorBadObj.mainLine;
values_for_ttest = current_field_values_matrix;
end


function [hours_from_start, p_per_block] = nestedTimewiseComparison(values_for_ttest, block_length_in_minutes, minimal_participans_required)

each_group_length = cellfun(@(mat) size(mat, 2), values_for_ttest);
ttest_length = min(each_group_length);
hours_from_start = (1:ttest_length) * block_length_in_minutes/60;

p_per_block = nan(ttest_length, 1);
for time_index = 1:ttest_length
    if sum(~isnan(values_for_ttest{1}(:, time_index))) < minimal_participans_required
        break;
    end
    if sum(~isnan(values_for_ttest{2}(:, time_index))) < minimal_participans_required
        break;
    end
    [~,p_per_block(time_index),~,~] = ttest2(values_for_ttest{1}(:, time_index), values_for_ttest{2}(:, time_index));
end
end


function nestedPrintSignifcancyInfo(p_per_block, hours_from_start, current_field)

significant_p = p_per_block <= 0.05;
significant_p_start = [significant_p(1); significant_p(2:end) & ~significant_p(1:end-1)];
significant_p_end = [significant_p(1:end-1) & ~significant_p(2:end); significant_p(end)];

significant_p_ranges = [find(significant_p_start), find(significant_p_end)];
significant_p_time_ranges = hours_from_start(significant_p_ranges);
fprintf('Comparing field %s: ', current_field);
if isempty(significant_p_time_ranges)
    fprintf(' None significant.');
else
    significant_p_time_durations = diff(significant_p_time_ranges, [], 2);
    fprintf(' Significant for %1.3f hours: ', sum(significant_p_time_durations));
    for range_index = 1:size(significant_p_time_ranges, 1)
        if range_index > 1
            fprintf(', ');
        end
        fprintf('%1.3f-%1.3f', significant_p_time_ranges(range_index, :));
    end
end
fprintf('\n');
end