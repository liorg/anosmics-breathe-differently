function figs = DisplaySleepVsWakePerGroup(experiment_results, fields_to_display, groups_colors, fields_prefixes)

if ~exist('fields_prefixes', 'var')
    fields_prefixes = cell(size(fields_to_display));
end

groups_titles = {'Normosmic', 'Anosmic'};

figs = cell(size(fields_to_display));
for fig_index = 1:numel(fields_to_display)
    current_fields = fields_to_display{fig_index};
    current_prefix = fields_prefixes{fig_index};
    figs{fig_index} = DisplayXYScatterPerGroup(experiment_results, current_fields, current_prefix, groups_titles, groups_colors);
end
figs = [figs{:}];