%% raincloud_plot - plots a half-violin, boxplot, and raw datapoints (1d scatter).

function h = raincloud_plot(X, color, box_on)

% ---------------------------- INPUT ----------------------------
%
% X - vector of data to be plotted, required.
%
% --------------------- ARGUMENTS ----------------------
%
% color             - color vector for rainclouds
% box_on             - logical to turn box plots on/off
%
% ---------------------------- OUTPUT ----------------------------
% h - figure handle to change more stuff
%


%% calculate kernel density
[f, Xi] = ksdensity(X);

%% density plot
h{1} = fill(Xi, f, color);
hold on

%% make some space under the density plot for the boxplot and raindrops
ax = gca;
current_yl = ax.YLim;
new_yl = [-current_yl(2) current_yl(2)];

%% Define width of boxplot
boxplot_width = new_yl(2) * 0.25;

%% Display raindrop (with jitter)
raindrop_jitter = (rand(size(X)) - 0.5) * boxplot_width;
drops_positions = raindrop_jitter + new_yl(1) / 2;

h{2} = scatter(X, drops_positions);
set(h{2}, 'SizeData', 10, 'MarkerFaceColor', color, 'MarkerEdgeColor', 'none');

%% Display boxplot, if needed
if box_on

    % info for making boxplot
    quartiles   = quantile(X, [0.25 0.75 0.5]);
    iqr = quartiles(2) - quartiles(1);
    iqr_limits = quartiles(1:2) + [-1, 1] .* 1.5 * iqr;
    whiskers = [min(X(X > iqr_limits(1))), max(X(X < iqr_limits(2)))];
    median_pos = quartiles(3);
    
    box_x_start = quartiles(1);
    box_y_start = new_yl(1)/2-(boxplot_width * 0.5);
    box_pos = [box_x_start, box_y_start, quartiles(2)-quartiles(1), boxplot_width];
    
    % median line
    h{3} = line([1, 1]*median_pos, [box_y_start, new_yl(1) / 2 + (boxplot_width * 0.5)]);
    
    % whiskers
    h{4} = line([quartiles(2), whiskers(2)], [1, 1]*new_yl(1)/2);
    h{5} = line([quartiles(1), whiskers(1)], [1, 1]*new_yl(1)/2);
    
    % 'box' of 'boxplot'
    h{6} = rectangle('Position', box_pos);
else    
    ax.YLim = new_yl;
end
