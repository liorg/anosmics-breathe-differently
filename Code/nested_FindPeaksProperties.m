function inhales_and_exhales_properties = nested_FindPeaksProperties(sampleLength, ...
    respirationData, peaksLocations)

%% Set default return-value
default_return_value = struct('PeakLocation',[], 'PeakValue',[], 'Volume',[],'Duration',[],'StartTime',[],'Latency',[],'NumberOfPeaks',[]);

%% Split respiration-data to connected-components, and calculate for each one the duration and the volume
thresholdForVolume = 0.1;
resamplingTimes = 4;
respirationDataInterpolated = interp(respirationData, resamplingTimes);
respirationDataInterpolatedAfterThreshold = respirationDataInterpolated;
respirationDataInterpolatedAfterThreshold(abs(respirationDataInterpolated) < thresholdForVolume) = 0;
respirationDataInterpolatedAfterThresholdPositive = respirationDataInterpolatedAfterThreshold > 0; 
respirationDataInterpolatedAfterThresholdNegative = respirationDataInterpolatedAfterThreshold < 0;

connectedCompPositive = bwconncomp(respirationDataInterpolatedAfterThresholdPositive);
connectedCompNegative = bwconncomp(respirationDataInterpolatedAfterThresholdNegative);
connectedCompPositiveProps = regionprops(connectedCompPositive, respirationDataInterpolatedAfterThreshold, {'Area', 'MeanIntensity', 'PixelIdxList'});
connectedCompNegativeProps = regionprops(connectedCompNegative, respirationDataInterpolatedAfterThreshold, {'Area', 'MeanIntensity', 'PixelIdxList'});
connectedCompProps = [connectedCompPositiveProps; connectedCompNegativeProps];

pixelsIndices = {connectedCompProps.PixelIdxList};
pixelsIndicesUpSamples = cellfun(@(indices) (indices-1)/resamplingTimes + 1, pixelsIndices, 'UniformOutput', false);
if isempty(pixelsIndices)
    inhales_and_exhales_properties = default_return_value;
    return
end

%% Map connected-component to peaks from input
pixelsIndicesUpSamplesRelevant = cellfun(@(indices) indices(mod(indices, 1) == 0), pixelsIndicesUpSamples, 'UniformOutput', false);
pixelsIndicesUpSamplesIndices = arrayfun(@(ind) ind*ones(size(pixelsIndicesUpSamplesRelevant{ind})), 1:numel(pixelsIndicesUpSamplesRelevant), 'UniformOutput', false);
pixelsIndicesUpsamplesTable = cat(2, cat(1, pixelsIndicesUpSamplesRelevant{:}), cat(1, pixelsIndicesUpSamplesIndices{:}));
if isempty(pixelsIndicesUpsamplesTable)
    inhales_and_exhales_properties = default_return_value;
    return
end
[~, connectedCompIndexWithPeak, peaksIndicesWithConnectedComp] = intersect(pixelsIndicesUpsamplesTable(:,1), peaksLocations);
% matchedConnectedCompTable = arrayfun(@(ind) pixelsIndicesUpsamplesTable(pixelsIndicesUpsamplesTable(:,1) == ind, 2), indicesBothLists, 'UniformOutput', false);
pixelsIndicesUpsamplesTableRelevant = pixelsIndicesUpsamplesTable(connectedCompIndexWithPeak, :);
connectedComponentsIndicesRelevant = pixelsIndicesUpsamplesTableRelevant(:, 2);
connectedCompWithMatchedPeakUniqueIndices = unique(connectedComponentsIndicesRelevant);
matchedPeaksIndices = arrayfun(@(ind) peaksIndicesWithConnectedComp(connectedComponentsIndicesRelevant == ind), connectedCompWithMatchedPeakUniqueIndices, 'UniformOutput', false);
matchedPeaksIndicesRespirationData = cellfun(@(indices) respirationData(peaksLocations(indices)), matchedPeaksIndices, 'UniformOutput', false);
numOfPeaksInEachConnectedComp = cellfun(@(data) numel(data), matchedPeaksIndicesRespirationData);
absRespirationDataInEachConnectedComp = cellfun(@(data) abs(data), matchedPeaksIndicesRespirationData, 'UniformOutput', false);
peakToUseInEachConnectedComp = cellfun(@(abs_data) find(abs_data == max(abs_data), 1), absRespirationDataInEachConnectedComp);
matchedPeaksIndicesToUse = arrayfun(@(connectedCompIndex) matchedPeaksIndices{connectedCompIndex}(peakToUseInEachConnectedComp(connectedCompIndex)), 1:numel(matchedPeaksIndices));
matchedConnectedComponents = connectedCompProps(connectedCompWithMatchedPeakUniqueIndices);

%% Get peaks values
matchedPeaksLocations = peaksLocations(matchedPeaksIndicesToUse);
peaksValues = respirationData(matchedPeaksLocations);
matchedPeaksLocationsInSeconds = (matchedPeaksLocations-1) * sampleLength;

%% Find crossing-thresholdForVolume points (for each connected component) using linear interpolation
matchedConnectedComponentsIndices = {matchedConnectedComponents.PixelIdxList};
matchedConnectedComponentsStartIndices = cellfun(@(indices) indices(1), matchedConnectedComponentsIndices);
matchedConnectedComponentsStartIndices_Start = max(1, matchedConnectedComponentsStartIndices-1);
dataAtStartIndices = respirationDataInterpolated(matchedConnectedComponentsStartIndices)';
thresholdWithSignOnStartIndices = sign(dataAtStartIndices) * thresholdForVolume;
dataAtStartIndicesReferencedToThreshold = dataAtStartIndices - thresholdWithSignOnStartIndices;
dataAtOneSampleBeforeStartIndicesReferencedToThreshold = respirationDataInterpolated(matchedConnectedComponentsStartIndices_Start)' - thresholdWithSignOnStartIndices;
crossingZeroBeforeStartIndices = (matchedConnectedComponentsStartIndices_Start.*dataAtStartIndicesReferencedToThreshold - matchedConnectedComponentsStartIndices.*dataAtOneSampleBeforeStartIndicesReferencedToThreshold)./(dataAtStartIndicesReferencedToThreshold-dataAtOneSampleBeforeStartIndicesReferencedToThreshold);

matchedConnectedComponentsEndIndices = cellfun(@(indices) indices(end), matchedConnectedComponentsIndices);
matchedConnectedComponentsEndIndices_Next = min(matchedConnectedComponentsEndIndices+1, numel(respirationDataInterpolated));
dataAtEndIndices = respirationDataInterpolated(matchedConnectedComponentsEndIndices)';
thresholdWithSignOnEndIndices = sign(dataAtEndIndices) * thresholdForVolume;
dataAtEndIndicesReferencedToThreshold = dataAtEndIndices - thresholdWithSignOnEndIndices;
dataAtOneSampleAfterEndIndicesReferencedToThreshold = respirationDataInterpolated(matchedConnectedComponentsEndIndices_Next)' - thresholdWithSignOnEndIndices;
crossingZeroAfterEndIndices = ((matchedConnectedComponentsEndIndices_Next).*dataAtEndIndicesReferencedToThreshold - matchedConnectedComponentsEndIndices.*dataAtOneSampleAfterEndIndicesReferencedToThreshold)./(dataAtEndIndicesReferencedToThreshold-dataAtOneSampleAfterEndIndicesReferencedToThreshold);

%% Get connected components values
durations = (crossingZeroAfterEndIndices - crossingZeroBeforeStartIndices) / resamplingTimes * sampleLength;
volumes = arrayfun(@(ind) trapz([crossingZeroBeforeStartIndices(ind); matchedConnectedComponentsIndices{ind}; crossingZeroAfterEndIndices(ind)], ...
    [0; respirationDataInterpolatedAfterThreshold(matchedConnectedComponentsIndices{ind}); 0]), 1:numel(durations)) / resamplingTimes * sampleLength;
startTimesUpsampled = (crossingZeroBeforeStartIndices(:)-1)/resamplingTimes + 1;
startTimesUpsampledInSeconds = (startTimesUpsampled - 1) * sampleLength;
latencies = matchedPeaksLocationsInSeconds - startTimesUpsampledInSeconds;

%% Summarize all the data
properties_table = array2table([matchedPeaksLocations(:), peaksValues(:), volumes(:), durations(:), startTimesUpsampledInSeconds(:), latencies(:), numOfPeaksInEachConnectedComp(:)], ...
    'VariableNames', {'PeakLocation', 'PeakValue', 'Volume', 'Duration', 'StartTime', 'Latency', 'NumberOfPeaks'});
inhales_and_exhales_properties = table2struct(properties_table);

end
