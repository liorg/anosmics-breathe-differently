function merged_figures = MergeFigures(sleep_vs_wake_figures, raincloud_figures, raincloud_panel_width)

number_of_figures = numel(sleep_vs_wake_figures);

merged_figures = cell(size(sleep_vs_wake_figures));
for field_index = 1:number_of_figures
    raincloud_figure = raincloud_figures(field_index);
    wake_raincloud = raincloud_figure.Children(1);
    sleep_raincloud = raincloud_figure.Children(2);
    
    xy_figure = sleep_vs_wake_figures(field_index);
    xy_axes = xy_figure.Children(end);
    xy_legend = xy_figure.Children(end-1);

    new_ylim = xy_axes.YLim;

    f = figure('Color', [1,1,1], 'Name', [xy_figure.Name, ' with raincloud']);
    f.Position([2,4]) = [100, f.Position(3)];
    merged_figures{field_index} = f;
    
    % Copy the xy-axes and set it to the upper-right area
    delete([f.Children]);
    copied_xy_ax_and_legend = copyobj([xy_axes, xy_legend], f);
    copied_xy_ax = copied_xy_ax_and_legend(1);
    copied_xy_ax.Units = 'points';
    copied_xy_ax.Position = round(copied_xy_ax.Position);
    copied_xy_ax.Position([1, 2, 4]) = [120, 120, copied_xy_ax.Position(3)];
    f.Position([3,4]) = 480;

    set(copied_xy_ax, 'XLim', new_ylim, 'YLim', new_ylim);
    
    
    % Copy the wake rain-cloud
    copied_raincloud_wake_ax = copyobj(wake_raincloud, f);
    copied_raincloud_wake_ax.Units = 'points';
    copied_raincloud_wake_ax.Box = 'off';
    copied_raincloud_wake_ax.LineWidth = copied_xy_ax.LineWidth;
    copied_raincloud_wake_ax.Position = copied_xy_ax.Position .* [1,1,0,1] + raincloud_panel_width * [-1.55, 0, 1, 0];
    copied_raincloud_wake_ax.XDir = 'reverse';
    copied_raincloud_wake_ax.XAxis.Visible = 'off';
    copied_raincloud_wake_ax.YAxis.Visible = 'off';
    copied_raincloud_wake_ax.Color(4) = 0;
    copied_raincloud_wake_ax.YLim = new_ylim;
    
    % Copy the sleep rain-cloud
    copied_raincloud_sleep_ax = copyobj(sleep_raincloud, f);
    copied_raincloud_sleep_ax.Units = 'points';
    copied_raincloud_sleep_ax.Box = 'off';
    copied_raincloud_sleep_ax.LineWidth = copied_xy_ax.LineWidth;
    copied_raincloud_sleep_ax.Position = copied_xy_ax.Position .* [1,1,1,0] + raincloud_panel_width * [0, -1.55, 0, 1];
    copied_raincloud_sleep_ax.YDir = 'reverse';
    copied_raincloud_sleep_ax.XAxis.Visible = 'off';
    copied_raincloud_sleep_ax.YAxis.Visible = 'off';
    copied_raincloud_sleep_ax.Color(4) = 0;
    copied_raincloud_sleep_ax.YLim = copied_raincloud_sleep_ax.XLim;
    copied_raincloud_sleep_ax.XLim = new_ylim;
    % Rotate
    raincloud_plot_objects = copied_raincloud_sleep_ax.Children;
    raincloud_plot_objects_types = arrayfun(@(item) item.Type, raincloud_plot_objects, 'UniformOutput', false);
    for i=1:numel(raincloud_plot_objects)
        if strcmp(raincloud_plot_objects_types{i}, 'rectangle')
            raincloud_plot_objects(i).Position = raincloud_plot_objects(i).Position([2,1,4,3]);
        else
            set(raincloud_plot_objects(i), 'XData', raincloud_plot_objects(i).YData, ...
                'YData', raincloud_plot_objects(i).XData);
        end
    end
end
end