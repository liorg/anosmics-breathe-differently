function allSessions = ReadAllSessionsLabcharts(labchartsPath)

%% Get relevant files
labchart_files = dir([labchartsPath, '*.mat']);
labchart_filenames = {labchart_files(~[labchart_files.isdir]).name}';

allSessions = struct('SubjectName', labchart_filenames);

%% Go thorugh all files
toReturnDataInCels = true;
startTime = tic;
for fileIndex = 1:numel(labchart_filenames)
    filename = labchart_filenames{fileIndex};
    subjectName = filename(1:find(filename == '_', 1)-1);
    if isempty(subjectName)
        subjectName = filename(1:find(filename == '-', 1)-1);
    end
    
    %% Load data from file and parse it
    expName = subjectName;
    full_filename = strcat(labchartsPath, filename);
    loadedData = load (full_filename);
    if isfield(loadedData, 'dataend')
        [dataToUse, tickRateInPart, channelsIDs] = nested_ParseLoadedDataInStructFormat(loadedData, expName, toReturnDataInCels);
    elseif isfield(loadedData, 'C1B1')
        [dataToUse, tickRateInPart, channelsIDs] = nested_ParseLoadedDataInChannelsFormat(loadedData, expName, toReturnDataInCels);
    else
        L.warn('Main', sprintf('Loaded file doesn''t contain dataend variable. Will skip it. Filename: %s', filename));
        return;
    end
    if isempty(dataToUse)
        return;
    end
    
    %% Change shape
    number_of_labchart_sessions = numel(dataToUse);
    labchart_sessions = cell(1, number_of_labchart_sessions);
    for session_index = 1:number_of_labchart_sessions
        labchart_session = struct('SubjectName', subjectName, 'SessionIndex', session_index, 'Tickrate', tickRateInPart);
        labchart_session.Time = (0:size(dataToUse{session_index}, 1)-1)'/tickRateInPart;
        for field_in_cell = fieldnames(channelsIDs)'
            field = field_in_cell{1};
            labchart_session.(field) = dataToUse{session_index}(:, channelsIDs.(field));
        end
        labchart_sessions{session_index} = labchart_session;
    end
    labchart_sessions = [labchart_sessions{:}];
    allSessions(fileIndex).LabchartSessions = labchart_sessions;
    
    fprintf('Finish subject %s in time %1.3f\n', subjectName, toc(startTime));
end

%% Remove too-short sessions, and sum the two respiration channels into one respiration channel, after subtracting the median from each channel
minimal_session_length_in_seconds = 60;
for fileIndex = 1:numel(allSessions)
    subjectSessions = allSessions(fileIndex).LabchartSessions;
    
    % Find each session duration
    sessions_durations_in_seconds = round(arrayfun(@(item) numel(item.Time)/item.Tickrate, subjectSessions));
    sessions_to_remove = sessions_durations_in_seconds < minimal_session_length_in_seconds;
    if any(sessions_to_remove)
        subjectSessions(sessions_to_remove) = [];
    end
    
    % For each session, sum the two channels
    for sessionIndex = 1:numel(subjectSessions)
        currentSession = subjectSessions(sessionIndex);
        respiration1Corrected = currentSession.Respiration1 - nanmedian(currentSession.Respiration1);
        respiration2Corrected = currentSession.Respiration2 - nanmedian(currentSession.Respiration2);
        totalRespiration = respiration1Corrected + respiration2Corrected;
        subjectSessions(sessionIndex).Respiration = totalRespiration;
        subjectSessions(sessionIndex).Respiration2ChannelsOriginal = cat(2, currentSession.Respiration1, currentSession.Respiration2);
        subjectSessions(sessionIndex).Respiration2Channels = cat(2, respiration1Corrected, respiration2Corrected);
    end
    subjectSessions = rmfield(subjectSessions, {'Respiration1', 'Respiration2'});
    
    % Store only the relevant session(s)
    allSessions(fileIndex).RelevantSessions = subjectSessions;
end

end

function [dataToUse, tickRateInPart, channelsIDs] = nested_ParseLoadedDataInStructFormat(loadedData, expName, toReturnDataInCels)
%% titles properties
respiration1Key = 'Resp1';
respiration2Key = 'Resp2';

titlesInFormat{1} = containers.Map({respiration1Key, respiration2Key}, {'Channel 6', 'Channel 7'});
titlesInFormat{2} = containers.Map({respiration1Key, respiration2Key}, {'Respiration1', 'Respiration2'});
titlesInFormat{3} = containers.Map({respiration1Key, respiration2Key}, {'Resp 1', 'Resp 2'});

%% data properties
relevantChannels = loadedData.dataend(:, 1) >= 0;

%% Remove empty channels
loadedData.datastart = loadedData.datastart(relevantChannels, :);
loadedData.dataend = loadedData.dataend(relevantChannels, :);
loadedData.titles = loadedData.titles(relevantChannels, :);
loadedData.rangemin = loadedData.rangemin(relevantChannels, :);
loadedData.rangemax = loadedData.rangemax(relevantChannels, :);
loadedData.unittextmap = loadedData.unittextmap(relevantChannels, :);
loadedData.samplerate = loadedData.samplerate(relevantChannels, :);
loadedData.firstsampleoffset = loadedData.firstsampleoffset(relevantChannels, :);

relevantChannels = loadedData.dataend(:, 1) >= 0;
relevantChannelsIndices = find(relevantChannels);

%% conver titles to cell-array
titlesCell = arrayfun(@(channelIdx) strtrim(loadedData.titles(channelIdx, :)), relevantChannelsIndices, 'UniformOutput', false);

%% select titles strings by current titles
titlesToUse = [];
for formatIndex = 1:numel(titlesInFormat)
    isMatchedFormat = all(cellfun(@(str) any(strcmp(str, titlesCell)), titlesInFormat{formatIndex}.values));
    if isMatchedFormat
        titlesToUse = titlesInFormat{formatIndex};
        break;
    end
end
if isempty(titlesToUse)
    error('Unknown titles strings for %s', expName);
end

%% If it's more than one block - Override blocktimes difference to be at most 5-minutes
number_of_channels = numel(titlesCell);
if numel(loadedData.datastart) ~= number_of_channels
    % More than one block
    maxAllowedDifferenceBetweenBlocksInSamples = 5*60*unique(loadedData.tickrate);
    loadedBlocktimes = loadedData.blocktimes;
    originalBlocktimesDiff = (loadedBlocktimes - loadedBlocktimes(1));
    samplesInEachPart = unique(loadedData.dataend - loadedData.datastart + 1, 'rows');
    maxAllowedSamplesFromStartOfRecording = [0, cumsum(samplesInEachPart(1:end-1))] + maxAllowedDifferenceBetweenBlocksInSamples;
    maxAllowedBlocktimeDiffFromStartOfRecording = maxAllowedSamplesFromStartOfRecording ./ loadedData.tickrate / (60*60*24);
    newBlocktimesDiff = min(originalBlocktimesDiff, maxAllowedBlocktimeDiffFromStartOfRecording);
    newBlockTimes = loadedBlocktimes(1) + newBlocktimesDiff;
    loadedData.blocktimes = newBlockTimes;
end

%% read the data and find the relevant part(s)
blocksToRemvoe = [];
relevantChannelsByTitleIndices = cellfun(@(str) find(strcmp(str, titlesCell)), titlesToUse.values);
[dataToUse, tickRateInPart] = ReadLabChartPartsAndMerge(loadedData.datastart, ...
    loadedData.dataend, loadedData.tickrate, loadedData.data, relevantChannelsByTitleIndices, [], [], ...
    loadedData.blocktimes, blocksToRemvoe, expName, toReturnDataInCels);

if isempty(dataToUse)
    L.Warn('Main', sprintf('Not found data in any part for %s\n', expName));
    return;
end

%% Find channel indices
channelsIDs = struct(...
    'Respiration1', find(strcmp(titlesToUse.keys, respiration1Key)), ...
    'Respiration2', find(strcmp(titlesToUse.keys, respiration2Key)) ...
    );
end

function [dataToUse, tickRateInPart, channelsIDs] = nested_ParseLoadedDataInChannelsFormat(loadedData, expName, toReturnDataInCels)

%% Initialize output variables
dataToUse = [];
tickRateInPart = [];
channelsIDs = struct();
L = log4m.getLogger('logfile.txt');

%% Convert channesl to matrix
loadedData_fields = sort(fieldnames(loadedData));
loadedData_fields_lengths = cellfun(@(field) numel(loadedData.(field)), loadedData_fields);
loadedData_fields_lengths_unique = unique(loadedData_fields_lengths);
if numel(loadedData_fields_lengths_unique) ~= 1
    L.error('Expecting all channels to have same length, but found %d different legnths for %s', ...
        numel(loadedData_fields_lengths_unique), expName);
    return;
end
dataToUse = nan(loadedData_fields_lengths_unique, numel(loadedData_fields));
for field_index = 1:numel(loadedData_fields)
    dataToUse(:, field_index) = loadedData.(loadedData_fields{field_index});
end

%% Get channels-IDs
error('Channel IDs should be corrected for this experiment!');
channelsIDs.RespirationRight = 6;
channelsIDs.RespirationLeft = 4;
channelsIDs.TRs = 3;
channelsIDs.Stimuli = 2;
channelsIDs.ButtonBox = 1;
channelsIDs.Channel5 = 5;
channelsIDs.PinkBall = 7;
channelsIDs.YellowBall = 8;

%% Set tick-rate
tickRateInPart = 1000;

%% Validate input
number_of_channels_IDs = numel(fieldnames(channelsIDs));
number_of_loaded_channels = size(dataToUse, 2);
if number_of_channels_IDs ~= number_of_loaded_channels
    L.warn('ParseLoadedDataInChannelsFormat', sprintf('Loaded file doesn''t contain expected number of channels. Contain %d, and expected is %d. Filename: %s', ...
        number_of_loaded_channels, number_of_channels_IDs, ...
        expName));
end

end
