function [raw_in_block]= data_into_blocks(block_length_in_minutes,sliding_window,data,SeparateNost,StartPoint,how_long, samplingRate)

if exist('how_long','var') && ~isempty(how_long)
    X=how_long;
else
    StartPoint=1;
    X=length(data);
end

if ~exist('samplingRate', 'var')
    samplingRate = 6;
end

if size(data, 2) == 1
    raw_data = data(StartPoint:end);
elseif size(data, 1) == 1
    raw_data = data(StartPoint:end)';
elseif SeparateNost
    raw_data = data(StartPoint:end,[2 4]);
else
    raw_data = data(StartPoint:end,2)+data(StartPoint:end,4);
end

% respirationData=Data_wake(:,2)+Data_wake(:,4);
data_idx=1:length(data);

N = round(block_length_in_minutes*60*samplingRate); % window size

if block_length_in_minutes<=1
    gap=0;
else
    Na=round(sliding_window*60*samplingRate);
    gap = (0:block_length_in_minutes/sliding_window-1)*Na;
end

if block_length_in_minutes<1
    Ysum=cumsum([0 N*ones(1,fix(X/N)),1+rem(X,N)]);
    i=1;
    raw_in_block = cell(length(Ysum)-1, 1);
    for ii=1:length(Ysum)-1
        block_ind= data_idx<Ysum(ii+1) & data_idx>(Ysum(ii)+1);
        raw_in_block{ii,i}=raw_data(block_ind,:);
    end

else
    ysum_length = 1 + fix(X/N) + 1;
    raw_in_block = cell(ysum_length-1, block_length_in_minutes);
    for i=1:block_length_in_minutes
        Ysum=cumsum([gap(i) N*ones(1,fix(X/N)),1+rem(X,N)]);
        for ii=1:length(Ysum)-1
            block_ind = data_idx<=Ysum(ii+1) & data_idx>=(Ysum(ii)+1);
            if sum(block_ind) < N
                continue; %% Not enough samples
            end
            raw_in_block{ii,i}=raw_data(block_ind,:)';
        end
    end
    % Remove rows from the bottom
    for ii = ysum_length-1:-1:1
        samples_in_current_row = cat(1, raw_in_block{ii, :});
        if size(samples_in_current_row, 1) == block_length_in_minutes
            break;
        elseif ~isempty(samples_in_current_row) && ysum_length < 10 && numel(samples_in_current_row) >= N
            break;
        end
        raw_in_block(ii, :) = [];
    end
end
end