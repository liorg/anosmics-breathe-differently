function respiratoryFeatures = RespiratoryFeaturesForSplittedData(respiratoryDataSplitted)

respiratoryFeatures = respiratoryDataSplitted;
start_time = tic;

for dataIndex=1:numel(respiratoryDataSplitted)
    %% Get breath-parameters for each data-block
    currentSplitted = respiratoryDataSplitted(dataIndex);
    if isempty(currentSplitted.Data)
        continue;
    end
    currentDataFirstVector = currentSplitted.Data(:, 1);
    [breathParameters, allPeaksCells] = AnalyzeRespirationByBlocks(currentDataFirstVector, currentSplitted.SamplingRate);
    fprintf('Finish analyzing respiratory #%d out of %d in time %2.3f.\n', dataIndex, numel(respiratoryDataSplitted), toc(start_time));

    %% Store breath-parameters in the output variable
    respiratoryFeatures(dataIndex).Data = breathParameters;
    respiratoryFeatures(dataIndex).Peaks = allPeaksCells;
end
fprintf('Finish analyzing respiration for all blocks in time %2.3f\n', toc(start_time));
end


function [breathParameters, allPeaksCells] = AnalyzeRespirationByBlocks(currentDataFirstVector, samplingRate)
currentDataFirstVectorSize = numel(currentDataFirstVector);
breathParametersCells = cell(currentDataFirstVectorSize, 1);
allPeaksCells = cell(currentDataFirstVectorSize, 1);
parfor i=1:length(breathParametersCells)
    TimeSeries = currentDataFirstVector{i};
    peaks = peaks_from_ts(TimeSeries', samplingRate);
    breathParametersCells{i} = calculate_z(peaks);
    allPeaksCells{i} = peaks;
end
breathParameters = [breathParametersCells{:}];
end
