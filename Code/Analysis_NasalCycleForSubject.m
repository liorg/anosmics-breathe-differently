function nasalCycleResults = ...
    Analysis_NasalCycleForSubject(Data, Sleep_Index, WakeUp_Index, sample_length)

%% Use default values
max_to_min = max(Data, [], 1) - min(Data, [], 1);
noiseThreshold = min(max_to_min)/100;

%% Get hilbert response
Fs = 1/sample_length;
Resp=hilbert24 (Data, Fs, noiseThreshold);

Start_sleep=find(Resp(3,:)>Sleep_Index,1,'first');
if isempty(Start_sleep)
    Start_sleep = size(Resp, 2);
end
Wake_up=find(Resp(3,:)<WakeUp_Index,1,'last');

%% Get cycle lengths
[Intervals_wake, Intervals_sleep, Int_R, Int_L, Total_Num_Dom_change, Num_dom_change_sleep, Num_dom_change_wake,...
    Total_avg_Int, Sleep_avg_Int, Wake_avg_Int, noisy_intervals_length, merged_dom_change,merged_Intervals,merged_R_L, dom_change, Intervals,...
    Intervals_wake_R_L, Intervals_sleep_R_L,~, ~, ~, ~]=...
    cycle_length_calc5 (Resp,Start_sleep, Wake_up, 0, 0, noiseThreshold); %#ok<ASGLU> % 4th and 5th parameters indicated if to include edge intervals

nasalCycleResults = struct();

nasalCycleResults.Intervals_wake = Intervals_wake;
nasalCycleResults.Intervals_sleep = Intervals_sleep;
nasalCycleResults.Right_intervals = Int_R;
nasalCycleResults.Left_intervals = Int_L;
nasalCycleResults.Count_Dominance_Change = Total_Num_Dom_change;
nasalCycleResults.Count_Dominance_Change_during_sleep = Num_dom_change_sleep;
nasalCycleResults.Count_Dominance_Change_during_wake = Num_dom_change_wake;
nasalCycleResults.Average_Interval_length = Total_avg_Int;
nasalCycleResults.Average_Interval_length_during_sleep = Sleep_avg_Int;
nasalCycleResults.Average_Interval_length_during_wake = Wake_avg_Int;

nasalCycleResults.All_Intervals_lengths = Intervals;
nasalCycleResults.noisy_intervals_length = noisy_intervals_length;
nasalCycleResults.merged_dom_change = merged_dom_change;
nasalCycleResults.merged_Intervals = merged_Intervals;
nasalCycleResults.merged_R_L = merged_R_L;
nasalCycleResults.index_in_minutes_of_Start_sleep = Start_sleep;
nasalCycleResults.index_in_minutes_of_Wake_up = Wake_up;
nasalCycleResults.Intervals_wake_R_L = Intervals_wake_R_L;
nasalCycleResults.Intervals_sleep_R_L = Intervals_sleep_R_L;

nasalCycleResults.R_intervals_during_wake = Intervals_wake(Intervals_wake_R_L>0);%R_Wake
nasalCycleResults.L_intervals_during_wake = Intervals_wake(Intervals_wake_R_L<0);%L_Wake
nasalCycleResults.R_intervals_during_sleep = Intervals_sleep(Intervals_sleep_R_L>0);%R_Sleep
nasalCycleResults.L_intervals_during_sleep = Intervals_sleep(Intervals_sleep_R_L<0);%L_Sleep

% one number variables
Laterality_Index=(Resp(1,:)-Resp(2,:))./(Resp(1,:)+Resp(2,:));
[NostrilCorrR, NostrilCorrP] = corrcoef(Resp(1,:), Resp(2,:));

nasalCycleResults.MeanLateralityIndex = mean(Laterality_Index);
nasalCycleResults.MeanAmplitudeLI = mean(abs(Laterality_Index));
nasalCycleResults.Nostril_Corr_RValue = NostrilCorrR(2,1);
nasalCycleResults.Nostril_Corr_PValue = NostrilCorrP(2,1);

% Sleep time parameters
if Start_sleep>Wake_up
    sleep_int=[1:Wake_up Start_sleep:length(Resp)];
    Resp_wake=Resp(:,Wake_up:Start_sleep);
else
    sleep_int=Start_sleep:Wake_up;
    Resp_wake=Resp;
    Resp_wake(:,sleep_int)=[];
end

LI_sleep=(Resp(1,sleep_int)-Resp(2,sleep_int))./(Resp(1,sleep_int)+Resp(2,sleep_int));
[NostrilCorrSleepR, NostrilCorrSleepP]=corrcoef(Resp(1,sleep_int), Resp(2,sleep_int));
if numel(NostrilCorrSleepR) == 1; NostrilCorrSleepR(2,1) = nan; end
if numel(NostrilCorrSleepP) == 1; NostrilCorrSleepP(2,1) = nan; end
nasalCycleResults.Mean_LI_sleep = mean(LI_sleep);
nasalCycleResults.Mean_amplitude_LI_sleep = mean(abs(LI_sleep));
nasalCycleResults.Nostril_corr_Sleep = NostrilCorrSleepR(2,1);
nasalCycleResults.Nostril_corr_Sleep_p = NostrilCorrSleepP(2,1);

% Wake time parameters
LI_wake=(Resp_wake(1,:)-Resp_wake(2,:))./(Resp_wake(1,:)+Resp_wake(2,:));
[NostrilCorrWakeR, NostrilCorrWakeP]=corrcoef(Resp_wake(1,:), Resp_wake(2,:));
nasalCycleResults.Mean_LI_wake = mean(LI_wake);
nasalCycleResults.Mean_amplitude_LI_wake = mean(abs(LI_wake));
nasalCycleResults.Nostril_corr_Wake = NostrilCorrWakeR(2,1);
nasalCycleResults.Nostril_corr_Wake_p = NostrilCorrWakeP(2,1);

end
