function f = CreateRocFigure(validationScores, actual_normosmics, actual_anosmics, expected_labels)
unique_score_thresholds = unique(validationScores(:));
unique_score_thresholds(isnan(unique_score_thresholds)) = [];
unique_score_thresholds = [unique_score_thresholds(1)-1; unique_score_thresholds; unique_score_thresholds(end)+1];
tnr = nan(size(unique_score_thresholds));
tpr = nan(size(unique_score_thresholds));
items_to_remove = isnan(validationScores(:, 1));
validation_scores_cleared = validationScores(~items_to_remove, :);
actual_normosmics_cleared = actual_normosmics(~items_to_remove);
actual_anosmics_cleared = actual_anosmics(~items_to_remove);
correct_types = expected_labels(~items_to_remove);
for th_index = 1:numel(unique_score_thresholds)
    currentTh = unique_score_thresholds(th_index);
    currentThLabeling = cell(size(correct_types));
    [currentThLabeling(validation_scores_cleared(:, 1) < currentTh)] = deal({'Normosmic'});
    [currentThLabeling(validation_scores_cleared(:, 1) >= currentTh)] = deal({'Anosmic'});
    currentCorrectPredictions = strcmp(currentThLabeling, correct_types);
    tnr(th_index) = sum(currentCorrectPredictions(actual_normosmics_cleared))/sum(actual_normosmics_cleared);
    tpr(th_index) = sum(currentCorrectPredictions(actual_anosmics_cleared))/sum(actual_anosmics_cleared);
end
f = figure('Color', [1,1,1]);
% scatter(tnr, tpr, 'o', 'Filled', 'SizeData', 60);
plot_obj = plot(tnr, tpr, 'o-', 'LineWidth', 2, 'MarkerSize', 6);
plot_obj.MarkerFaceColor = plot_obj.Color;
hold on;
plot([0, 1], [1, 0], 'k--', 'LineWidth', 1);
ax = gca;
ax.Box = 'off';
ax.FontSize = 14;
axis([0, 1, 0, 1]);
xlabel('True negative rate (TNR)');
ylabel('True positive rate (TPR)');
auc = trapz(tnr, tpr);
leg_obj = legend(['ROC (AuC=', num2str(auc*100, '%2.2f'), '%)'], 'Randomized predictor', 'Location', 'SouthWest', 'Box', 'off');
