function peaksAnalysis = AnalysisPeaksPerSubject(subjectPeaks, ...
    block_length_in_minutes, min_peaks_in_block, max_peaks_in_block, ...
    WAKE_SLEEP_ENUM, wake_sleep_options)

%% Use variables for current subject
inhale_peaks = subjectPeaks.InhalePeaks;
inhale_locations = subjectPeaks.InhalePeaksTimes;
exhale_peaks = subjectPeaks.ExhalePeaks;
exhale_locations = subjectPeaks.ExhalePeaksTimes;
sampleLength = subjectPeaks.SampleLength;
numberOfSamples = subjectPeaks.NumberOfSamples;
wake_up_time_seconds = subjectPeaks.WakeUpSeconds;
sleep_time_seconds = subjectPeaks.SleepTimeSeconds;

blocks_offsets_in_minutes = 0;

peaksAnalysis = cell(numel(blocks_offsets_in_minutes), numel(wake_sleep_options));

%% Find normalize values - median-inhale to median-exhale ratio should be 2, with the center at 0
median_inhale_peak = median(inhale_peaks);
median_exhale_peak = median(exhale_peaks);

%% Normalize peaks
inhale_peaks = inhale_peaks/median_inhale_peak;
exhale_peaks = exhale_peaks/abs(median_exhale_peak);

%% Split maximums and minimus to given minutes blocks
block_length_in_seconds = round(block_length_in_minutes*60);
block_length_in_samples = round(block_length_in_seconds / sampleLength);
num_of_blocks = ceil(numberOfSamples/block_length_in_samples);
blocksIndices = 1:num_of_blocks;

for blocks_offset_in_minutes_index = 1:numel(blocks_offsets_in_minutes)
    blocks_offset_in_minutes = blocks_offsets_in_minutes(blocks_offset_in_minutes_index);
    
    inhale_locations_block_index = floor((inhale_locations - blocks_offset_in_minutes * 60)/ block_length_in_seconds) + 1;
    inhale_peaks_in_block_cell_arr = arrayfun(@(ind) inhale_peaks(inhale_locations_block_index == ind), blocksIndices, 'UniformOutput', false)';
    inhale_peaks_in_block_avg = cellfun(@mean, inhale_peaks_in_block_cell_arr);
    inhale_peaks_in_block_std = cellfun(@std, inhale_peaks_in_block_cell_arr);
    num_inhale_peaks_in_block = cellfun(@numel, inhale_peaks_in_block_cell_arr);
    inhale_times_diffs_in_block_cell_arr = arrayfun(@(ind) diff(inhale_locations(inhale_locations_block_index == ind)), blocksIndices, 'UniformOutput', false)';
    inhale_times_diffs_in_block_avg = cellfun(@mean, inhale_times_diffs_in_block_cell_arr);
    inhale_times_diffs_in_block_std = cellfun(@std, inhale_times_diffs_in_block_cell_arr);
    inhale_peaks_to_remove = num_inhale_peaks_in_block < min_peaks_in_block | num_inhale_peaks_in_block > max_peaks_in_block;
    inhale_peaks_in_block_avg(inhale_peaks_to_remove) = nan;
    inhale_peaks_in_block_std(inhale_peaks_to_remove) = nan;
    inhale_times_diffs_in_block_avg(inhale_peaks_to_remove) = nan;
    inhale_times_diffs_in_block_std(inhale_peaks_to_remove) = nan;
    
    exhale_locations_block_index = floor((exhale_locations - blocks_offset_in_minutes * 60) / block_length_in_seconds) + 1;
    exhale_peaks_in_block_cell_arr = arrayfun(@(ind) -exhale_peaks(exhale_locations_block_index == ind), blocksIndices, 'UniformOutput', false)';
    exhale_peaks_in_block_avg = cellfun(@mean, exhale_peaks_in_block_cell_arr);
    exhale_peaks_in_block_std = cellfun(@std, exhale_peaks_in_block_cell_arr);
    num_exhale_peaks_in_block = cellfun(@numel, exhale_peaks_in_block_cell_arr);
    exhale_times_diffs_in_block_cell_arr = arrayfun(@(ind) diff(exhale_locations(exhale_locations_block_index == ind)), blocksIndices, 'UniformOutput', false)';
    exhale_times_diffs_in_block_avg = cellfun(@mean, exhale_times_diffs_in_block_cell_arr);
    exhale_times_diffs_in_block_std = cellfun(@std, exhale_times_diffs_in_block_cell_arr);
    exhale_peaks_to_remove = num_exhale_peaks_in_block < min_peaks_in_block | num_exhale_peaks_in_block > max_peaks_in_block;
    exhale_peaks_in_block_avg(exhale_peaks_to_remove) = nan;
    exhale_peaks_in_block_std(exhale_peaks_to_remove) = nan;
    exhale_times_diffs_in_block_avg(exhale_peaks_to_remove) = nan;
    exhale_times_diffs_in_block_std(exhale_peaks_to_remove) = nan;
    
    blocksStarts = (blocksIndices-1) * block_length_in_seconds;
    
    %% Use only sleep/wake/both
    for wake_sleep_option_index = 1:numel(wake_sleep_options)
        wake_sleep_option = wake_sleep_options(wake_sleep_option_index);
        switch wake_sleep_option
            case WAKE_SLEEP_ENUM.WAKE_AND_SLEEP
                relevant_blocks_indices = true(size(blocksStarts));
                
            case WAKE_SLEEP_ENUM.WAKE_ONLY
                if isnan(sleep_time_seconds)
                    number_of_blocks = numberOfSamples*sampleLength / 60 / block_length_in_minutes;
                    relevant_blocks_indices = blocksIndices <= number_of_blocks;
                 elseif wake_up_time_seconds > sleep_time_seconds
                    relevant_blocks_indices = blocksStarts + block_length_in_seconds < sleep_time_seconds | blocksStarts > wake_up_time_seconds;
                else
                    relevant_blocks_indices = blocksStarts + block_length_in_seconds < sleep_time_seconds & blocksStarts > wake_up_time_seconds;
                end
                
            case WAKE_SLEEP_ENUM.SLEEP_ONLY
                if wake_up_time_seconds > sleep_time_seconds
                    relevant_blocks_indices = blocksStarts > sleep_time_seconds & blocksStarts + block_length_in_seconds < wake_up_time_seconds;
                else
                    relevant_blocks_indices = blocksStarts > sleep_time_seconds | blocksStarts + block_length_in_seconds < wake_up_time_seconds;
                end
                
            otherwise
                error('Unhandled wake-sleep-option: %d', wake_sleep_option);
        end
        
        %% Save results to structure
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index} = struct();
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.BlockLength = block_length_in_minutes;
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.BlocksStarts = blocksStarts(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.InhalePeaksCount = num_inhale_peaks_in_block(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.InhalePeaksAvg = inhale_peaks_in_block_avg(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.InhalePeaksStd = inhale_peaks_in_block_std(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.InhaleTimesDiffsAvg = inhale_times_diffs_in_block_avg(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.InhaleTimesDiffsStd = inhale_times_diffs_in_block_std(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.ExhalePeaksCount = num_exhale_peaks_in_block(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.ExhalePeaksAvg = exhale_peaks_in_block_avg(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.ExhalePeaksStd = exhale_peaks_in_block_std(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.ExhaleTimesDiffsAvg = exhale_times_diffs_in_block_avg(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.ExhaleTimesDiffsStd = exhale_times_diffs_in_block_std(relevant_blocks_indices);
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.WakeSleepOption = wake_sleep_option;
        peaksAnalysis{blocks_offset_in_minutes_index, wake_sleep_option_index}.BlocksOffsetInMinutes = blocks_offset_in_minutes;

    end
end
