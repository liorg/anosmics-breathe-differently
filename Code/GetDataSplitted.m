function splittedData = GetDataSplitted(Data, sleep_time_index, wake_up_index, sampleLength, block_length_in_minutes)

%% Default values
if ~exist('block_length_in_minutes', 'var')
    block_length_in_minutes=5;
end

%% Extract wake-sleep raw
timings_per_subj = struct('Start_sleep', sleep_time_index, ...
    'Wake_up', wake_up_index, ...
    'NapStart', nan, 'NapEnd', nan);
CutEdge=1; %exclude morning shorter than CutEdge (in Hours)
ExcludeNap=true; %exclude nap
SeparateNost = false;
% normalize data
NormDataToUse = Data;
NormDataToUse = NormDataToUse / nanstd(NormDataToUse); %instead of z-score - we want the median to stay 0
if round(sampleLength, 3) == 0.167
    samplingRate = 6;
else
    samplingRate = 1/sampleLength;
end
if isnan(sleep_time_index)
    Data_wake = NormDataToUse;
    Data_sleep = [];
else
    [Data_wake, Data_sleep] = extract_wake_sleep_raw(NormDataToUse, timings_per_subj, CutEdge, ExcludeNap, samplingRate);
end

sliding_window_in_minutes=1;

raw_in_block_wake = data_into_blocks(block_length_in_minutes, sliding_window_in_minutes, Data_wake, SeparateNost, [], [], samplingRate);
if isempty(Data_sleep)
    raw_in_block_sleep = {};
else
    raw_in_block_sleep = data_into_blocks(block_length_in_minutes, sliding_window_in_minutes, Data_sleep, SeparateNost, [], [], samplingRate);
end

splittedData = struct('Arousal', {'Wake', 'Sleep'}, ...
    'SampleLength', sampleLength, ...
    'SamplingRate', samplingRate, ...
    'SamplesInEachBlock', numel(raw_in_block_wake{1}), ...
    'NormalizationRatio', 1./nanstd(Data), ...
    'Data', {raw_in_block_wake, raw_in_block_sleep});

end

