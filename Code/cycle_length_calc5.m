function [Intervals_wake, Intervals_sleep, Int_R, Int_L, Total_Num_Dom_change, Num_dom_change_sleep, Num_dom_change_wake,...
    Total_avg_Int, Sleep_avg_Int, Wake_avg_Int, noisy_intervals_length, merged_dom_change,merged_Intervals,merged_R_L, dominance_change_indices, ...
    Intervals, Intervals_wake_R_L, Intervals_sleep_R_L, FromSleepToWake_percent, FromWakeToSleep_percent, FromSleepToWake, FromWakeToSleep]...
 = cycle_length_calc5 (Resp,Start_sleep, Wake_up, withStartEdge, withEndEdge, responseNoiseThreshold)

rightToLeftDiff = Resp(1,:)-Resp(2,:);
reponseNoiseMap = (Resp(1,:) < responseNoiseThreshold) & (Resp(2,:) < responseNoiseThreshold);
total_time = size(Resp,2);

rightPerFrame = double(rightToLeftDiff>0); % 1 if right, 0 if left
%% Clean response-noise frames
response_noise_interval_threshold = 60; % minutes
rightPerFrameUpdated = Update_Right_Nostril_By_Response_Noise(rightPerFrame, reponseNoiseMap, response_noise_interval_threshold);

dominanceChangeMap = diff(rightPerFrame);% find the derivative where there is dominance change 1 starts right block -1 starts left block
checkRightPerFrameForNan = diff(rightPerFrameUpdated);
dominance_change_indices_from_map = find(dominanceChangeMap ~= 0);
dominance_change_due_to_response_noise = find(isnan(checkRightPerFrameForNan));
if ~isempty(dominance_change_due_to_response_noise)
    dominance_change_due_to_response_noise(dominance_change_due_to_response_noise < dominance_change_indices_from_map(1)) = [];
    dominance_change_due_to_response_noise(dominance_change_due_to_response_noise > dominance_change_indices_from_map(end)) = [];
end


dominance_change_indices = union(dominance_change_due_to_response_noise, dominance_change_indices_from_map);

Intervals = diff([0, dominance_change_indices, total_time]);

if ~withStartEdge
    Intervals = Intervals(2:end); %discard information of first change
else
    dominance_change_indices = [0 dominance_change_indices];
end

if ~withEndEdge
    Intervals = Intervals(1:end-1); %discard information of last change
    dominance_change_indices = dominance_change_indices(1:end-1); %discard information of last change
end

R_L_after_dominance_change = 2*rightPerFrame(dominance_change_indices + 1) - 1; % 1 for right, -1 for left

[noisy_intervals_length,merged_dom_change,merged_Intervals,merged_R_L] = ...
    calc_average_relevant_interval(Intervals,dominance_change_indices,R_L_after_dominance_change);

% Number of dominance changes
Total_Num_Dom_change = numel(merged_dom_change);
if Start_sleep <= Wake_up
    dominanceChangeInSleepMap = merged_dom_change>Start_sleep & merged_dom_change<Wake_up;
else
    dominanceChangeInSleepMap = merged_dom_change>Start_sleep | merged_dom_change<Wake_up;
end

Num_dom_change_sleep = sum(dominanceChangeInSleepMap);
Num_dom_change_wake = sum(~dominanceChangeInSleepMap);
    

Total_avg_Int = (total_time-noisy_intervals_length)/(Total_Num_Dom_change+1);

if Start_sleep < Wake_up
    Wake_avg_Int = (Start_sleep+(total_time-Wake_up))/(Num_dom_change_wake+1);
    Sleep_avg_Int = (Wake_up-Start_sleep)/(Num_dom_change_sleep+1);
else
    Wake_avg_Int = (Start_sleep-Wake_up)/(Num_dom_change_wake+1);
    Sleep_avg_Int = (Wake_up+(total_time-Start_sleep))/(Num_dom_change_sleep+1);
end

Intervals_sleep = merged_Intervals(dominanceChangeInSleepMap);
Intervals_sleep_R_L = merged_R_L(dominanceChangeInSleepMap);
Intervals_wake = merged_Intervals(~dominanceChangeInSleepMap);
Intervals_wake_R_L = merged_R_L(~dominanceChangeInSleepMap);

Int_R = merged_Intervals(merged_R_L>0);
Int_L = merged_Intervals(merged_R_L<0);


%%%%%%%%%%%
% decide marginal sleep-wake intervals
% First- intervals that Start_sleep falls in the middle of them
wake_ind = find(merged_dom_change<Start_sleep);
if isempty(wake_ind)
    FromWakeToSleep_percent = [];
    FromWakeToSleep = [];
else
    last_wake = wake_ind(end);
    Int_length = merged_Intervals(last_wake);

    if (Start_sleep-merged_dom_change(last_wake))/Int_length >= 0.5
        FromWakeToSleep_percent = [];
        FromWakeToSleep = [];
    else
        FromWakeToSleep_percent = (Start_sleep-merged_dom_change(last_wake))/Int_length;

        Intervals_sleep = [merged_Intervals(last_wake) Intervals_sleep ];
        Intervals_sleep_R_L = [ merged_R_L(last_wake) Intervals_sleep_R_L];
        to_del = find(Intervals_wake == merged_Intervals(last_wake),1,'first');
        FromWakeToSleep = Intervals_wake(to_del);
        Intervals_wake(to_del) = [];
        Intervals_wake_R_L(to_del) = [];
    end
end
    
% Second- intervals that Wake_up falls in the middle of them

sleep_ind = find(merged_dom_change<Wake_up);
%make sure the relevant interval is still in sleep time (because possible
%nights are with no dominance change at all
if ~isempty(sleep_ind) && ((merged_dom_change(sleep_ind(end))>Start_sleep) || (Wake_up<Start_sleep))
    Int_length2 = merged_Intervals(sleep_ind(end));
    if (Wake_up-merged_dom_change(sleep_ind(end)))/Int_length2<0.5
        FromSleepToWake_percent = (Wake_up-merged_dom_change(sleep_ind(end)))/Int_length2;
        
        Intervals_wake = [Intervals_wake merged_Intervals(sleep_ind(end))];
        Intervals_wake_R_L = [Intervals_wake_R_L merged_R_L(sleep_ind(end))];
        to_del = find(Intervals_sleep == merged_Intervals(sleep_ind(end)),1,'first');
        FromSleepToWake = Intervals_sleep(to_del);
        Intervals_sleep(to_del) = [];
        Intervals_sleep_R_L(to_del) = [];
        
    else
        FromSleepToWake_percent = [];
        FromSleepToWake = [];
    end
else
    FromSleepToWake_percent = [];
    FromSleepToWake = [];
end

%
% sleep_margin = 60;% allow sleep margin min of cycle change before or after sleep
%
%    % If sleep interval started a bit before start sleep
%     wake_ind = find(merged_dom_change<Start_sleep);
%     last_wake = wake_ind(end);
%     if merged_dom_change(last_wake)>Start_sleep-sleep_margin;
%         Intervals_sleep = [Intervals_sleep merged_Intervals(last_wake)];
%         to_del = find(Intervals_wake == merged_Intervals(last_wake),1,'first');
%         Intervals_wake(to_del) = [];
%     end
%
%    % If wake interval started a bit before wake up
%    sleep_ind = find(merged_dom_change<Wake_up);
%    last_sleep = sleep_ind(end);
%     if merged_dom_change(last_sleep)>Wake_up-sleep_margin;
%         Intervals_wake = [Intervals_wake merged_Intervals(last_sleep)];
%         to_del = find(Intervals_sleep == merged_Intervals(last_sleep),1,'first');
%         Intervals_sleep(to_del) = [];
%     end

end




function [noisy_intervals_length,dom_change_indices,Intervals,R_L] = ...
    calc_average_relevant_interval(Intervals,dom_change_indices,R_L)

small_interval_threshold = 15; % minutes
noisy_interval_threshold = 30; % minutes

%% Merge small intervals
small_intervals_inds = find(Intervals < small_interval_threshold);
[Intervals_merged_small, small_end_ind] = Merge_Small_Intervals(Intervals, small_intervals_inds);

%% Merge noisy short intervals
numOfChanges = numel(dom_change_indices);
relevant_intervals = setdiff(1:numOfChanges, small_intervals_inds);
[Intervals_Small_Noisy_Intervals_Merged, short_interval_inds, noisy_intervals_length] = Merge_Small_Noisy_Intervals(Intervals, ...
    Intervals_merged_small, relevant_intervals, small_end_ind, noisy_interval_threshold);

%% compute the long intervals which need to be merge
first_relevant_interval = min(relevant_intervals);
merge_interval_inds = Find_Merge_Indices(small_end_ind, short_interval_inds, first_relevant_interval, numOfChanges);

%% consider for merge only two intervals which agree on R/L:
merge_interval_inds = Remove_Intervals_Which_Not_Agree_Dominance(merge_interval_inds, relevant_intervals, R_L);
relevant_intervals = setdiff(relevant_intervals,merge_interval_inds);

%% calc merged interval length
Intervals_Noisy_Intervals_Merged = Merge_Noisy_Intervals(Intervals_Small_Noisy_Intervals_Merged, merge_interval_inds, relevant_intervals);

%% choose only the relevant (non noisy) intervals
Intervals = Intervals_Noisy_Intervals_Merged(relevant_intervals);
dom_change_indices = dom_change_indices(relevant_intervals);
R_L = R_L(relevant_intervals);

end


function [Intervals_small, small_end_ind] = Merge_Small_Intervals(Intervals, small_intervals_inds)
if isempty(small_intervals_inds)
    Intervals_small = Intervals;
    small_end_ind = [];
else
    small_intervals_inds_new_section_map = diff(small_intervals_inds) > 1;
    small_end_ind = small_intervals_inds([small_intervals_inds_new_section_map, true]);
    
    % Allocate memory
    num_of_merged_intervals = sum(small_intervals_inds_new_section_map) + 1;
    Intervals_small = nan(1, num_of_merged_intervals);
    
    % Unify neighbouring small intervals
    i = 1;
    j = 0;
    while i <= numel(small_intervals_inds)
        j = j+1;
        Intervals_small(j) = Intervals(small_intervals_inds(i));
        
        while i < numel(small_intervals_inds) && ~(small_intervals_inds_new_section_map(i))
            i = i+1;
            Intervals_small(j) = Intervals_small(j) + Intervals(small_intervals_inds(i));
        end
        i = i+1;
    end
end
end


function rightPerFrame = Update_Right_Nostril_By_Response_Noise(rightPerFrame, reponseNoiseMap, response_noise_interval_threshold)
reponseNoiseMapDiff = diff(reponseNoiseMap);
responseNoiseStartIndices = find(reponseNoiseMapDiff == 1);
responseNoiseEndIndices = find(reponseNoiseMapDiff == -1) + 1; % +1 - because diff is shifting to the left
for responseNoiseIndex = 1:numel(responseNoiseStartIndices)
    currentResponseNoiseStart = responseNoiseStartIndices(responseNoiseIndex);
    if currentResponseNoiseStart > 1
        rightPerFrameOnStart = rightPerFrame(currentResponseNoiseStart-1);
    else
        rightPerFrameOnStart = 0.2;
    end
    
    if responseNoiseIndex <= numel(responseNoiseEndIndices)
        currentResponseNoiseEnd = responseNoiseEndIndices(responseNoiseIndex);
        rightPerFrameOnEnd = rightPerFrame(currentResponseNoiseEnd);
    else
        currentResponseNoiseEnd = numel(reponseNoiseMap) + 1;
        rightPerFrameOnEnd = 1-rightPerFrameOnStart;
    end
    
    currentResponseNoiseInterval = currentResponseNoiseEnd - currentResponseNoiseStart;
    if currentResponseNoiseInterval > response_noise_interval_threshold
        rightPerFrame(currentResponseNoiseStart:currentResponseNoiseEnd) = nan;
    elseif rightPerFrameOnStart ~= rightPerFrameOnEnd
        rightPerFrame(currentResponseNoiseStart:currentResponseNoiseEnd) = nan;
    else
        rightPerFrame(currentResponseNoiseStart:currentResponseNoiseEnd) = rightPerFrameOnStart;
    end
end
end


function [Intervals, short_interval_inds, noisy_intervals_length] = Merge_Small_Noisy_Intervals(Intervals, ...
    Intervals_merged_small, relevant_intervals, small_end_ind, noisy_interval_threshold)

short_intervals_map = Intervals_merged_small <= noisy_interval_threshold;
noisy_intervals_length = sum(Intervals_merged_small(~short_intervals_map));
short_interval_inds = find(short_intervals_map);

% Add the very short intervals to prev interval:
for i = 1:numel(short_interval_inds)
    current_short_interval_ind = short_interval_inds(i);
    interval_ind = relevant_intervals(find(relevant_intervals < small_end_ind(current_short_interval_ind), 1, 'last'));
    if isempty(interval_ind)
        noisy_intervals_length = noisy_intervals_length + Intervals_merged_small(current_short_interval_ind);
    else
        Intervals(interval_ind) = Intervals(interval_ind) + Intervals_merged_small(current_short_interval_ind);
    end
end
end


function merge_interval_inds = Find_Merge_Indices(small_end_ind, short_interval_inds, first_relevant_interval, numOfChanges)
if ~isempty(short_interval_inds) && small_end_ind(short_interval_inds(end)) == numOfChanges
    short_interval_inds = short_interval_inds(1:end-1);
end
merge_interval_inds = sort(small_end_ind(short_interval_inds)+1,'ascend');
if ~isempty(merge_interval_inds) && first_relevant_interval >= merge_interval_inds(1)%if all first intervals where short
    merge_interval_inds(1) = [];
end
end


function merge_interval_inds = Remove_Intervals_Which_Not_Agree_Dominance(merge_interval_inds, relevant_intervals, R_L)
remove_merge = false(size(merge_interval_inds));
for i = 1:numel(merge_interval_inds)
    interval_ind = relevant_intervals(find(relevant_intervals < merge_interval_inds(i), 1, 'last' ));
    if R_L(interval_ind)~= R_L(merge_interval_inds(i))
        remove_merge(i) = true;
    end
end
merge_interval_inds(remove_merge) = [];
end


function Intervals = Merge_Noisy_Intervals(Intervals, merge_interval_inds, relevant_intervals)
for i = 1:numel(merge_interval_inds)
    interval_ind = relevant_intervals(find(relevant_intervals < merge_interval_inds(i), 1, 'last' ));
    Intervals(interval_ind) = Intervals(interval_ind) + Intervals(merge_interval_inds(i));
end
end
