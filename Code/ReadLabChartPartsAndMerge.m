function [dataToUse, tickRateInPart, toUse] = ReadLabChartPartsAndMerge(dataStart, ...
dataEnd, tickRates, dataValues, relevantChannelsByTitleIndices, ...
triggerChannelId, triggerDiffMarks, blockTimes, blocksToRemvoe, expName, toReturnDataInCels)

if ~exist('toReturnDataInCels', 'var')
    toReturnDataInCels = false;
end

numOfParts = size(dataEnd, 2);

UseTriggerToDecideWhichPartsToUse = exist('triggerChannelId', 'var') && ~isempty(triggerChannelId) && exist('triggerDiffMarks', 'var') && ~isempty(triggerDiffMarks);
UseBlockTimes = exist('blockTimes', 'var');

%% read the data and find the relevant part(s)
toUse = false(1, numOfParts);
samplesPerPart = zeros(1, numOfParts);
data_s = cell(1, numOfParts);
for i=1:numOfParts
    samples_per_channel = arrayfun(@(channdleIndex) dataEnd(channdleIndex, i) - dataStart(channdleIndex, i) + 1, relevantChannelsByTitleIndices);
    samples_per_channel_unique = unique(samples_per_channel);
    if numel(samples_per_channel_unique) ~= 1
        error('Number of samples per channel should be the same for all channels.');
    end
    samplesPerPart(i) = samples_per_channel_unique;
    data_indices_to_use = arrayfun(@(channdleIndex) dataStart(channdleIndex, i):dataEnd(channdleIndex, i), relevantChannelsByTitleIndices, 'UniformOutput', false);
    data_to_reshape = dataValues(cat(1, data_indices_to_use{:}));
    data_s{i} = data_to_reshape';
%     fprintf('Median of data for part #%d in "%s" is %1.3f\n', i, expName, median(data_s{i}));
    
    if UseTriggerToDecideWhichPartsToUse
        triggerChannel = data_s{i}(:,triggerChannelId);
        triggerChannelUp = FindUpOfChannel(triggerChannel, triggerDiffMarks);
        toUse(i) = any(triggerChannelUp);
    end
end

%% merge data
if ~UseTriggerToDecideWhichPartsToUse
    if UseBlockTimes && numOfParts > 1
        blockTimes(blocksToRemvoe) = min(blockTimes(setdiff(1:numOfParts, blocksToRemvoe)));
        blockTimesFirstDec = find(diff(blockTimes) < 0, 1);
        if ~isempty(blockTimesFirstDec)
            blockTimes(blockTimesFirstDec+1:end) = blockTimes(blockTimesFirstDec);
        end
        [data_s{blocksToRemvoe}] = deal([]);
        samplesPerPart(blocksToRemvoe) = 0;
        
        minuesFromFirstBlockTime = (blockTimes - blockTimes(1))*24*60;
        samplesFromStartOfRecording = round(minuesFromFirstBlockTime * 60 .* tickRates);
        samplesToPadEachPart = [samplesFromStartOfRecording(2:end)-cumsum(samplesPerPart(1:end-1)), 0];
        data_s_padded = arrayfun(@(ind) [data_s{ind}; nan(samplesToPadEachPart(ind), size(data_s{ind}, 2))], 1:numOfParts, 'UniformOutput', false);
        data_s = data_s_padded;
        toUse = true(size(data_s));
    else
        [~, longestPart] = max(samplesPerPart);
        toUse(longestPart) = true;
        data_s = data_s(longestPart);
        tickRates = tickRates(longestPart);
    end
end

if ~any(toUse)
    dataToUse = [];
    tickRateInPart = [];
    return;
end

if toReturnDataInCels
    dataToUse = data_s;
else
    dataToUse = cat(1, data_s{:});
end
tickRateInPart = unique(tickRates(:));

if numel(tickRateInPart) ~= 1
    error('Tick-rate should be equal in all relevant parts');
end