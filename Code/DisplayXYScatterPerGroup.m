function fig = DisplayXYScatterPerGroup(experiment_results, current_fields, current_prefix, groups_titles, groups_colors)

if ~exist('current_prefix', 'var')
    current_prefix = {};
end

relevant_participants_grouping = {experiment_results.Group};
numOfUniqueGroups = numel(groups_titles);
each_group_indices = cell(size(groups_titles));
for i=1:numOfUniqueGroups
    each_group_indices{i} = strcmp(relevant_participants_grouping, groups_titles{i});
end

x_offsets_relevant_participants = 0.075*linspace(-1, 1, numel(relevant_participants_grouping))';
missing_fields = ~isfield(experiment_results, current_fields);
if any(missing_fields)
    fprintf('Missing field(s): %s\n', strjoin(current_fields(missing_fields), '; '));
    return;
end

both_fields_values = struct();
ends_with_sleep_vs_wake = all(endsWith(current_fields, 'leep') | endsWith(current_fields, 'ake'));
starts_with_sleep_vs_wake = all(startsWith(current_fields, 'Sleep') | startsWith(current_fields, 'Wake'));
is_displaying_sleep_vs_wake = ends_with_sleep_vs_wake || starts_with_sleep_vs_wake;
if ends_with_sleep_vs_wake
    for idx=1:numel(current_fields)
        current_fieldname = current_fields{idx};
        current_phase = current_fieldname(find(current_fieldname == '_', 1, 'last')+1:end);
        current_phase(1) = upper(current_phase(1));
        both_fields_values.(current_phase) = [experiment_results.(current_fieldname)];
    end
    
    % Find title
    current_field_for_label = current_fieldname(1:find(current_fieldname == '_', 1, 'last')-1);
    current_field_for_label = strrep(current_field_for_label, '_', ' ');
    current_field_for_label = strrep(current_field_for_label, ' during', '');
else
    for idx=1:numel(current_fields)
        current_fieldname = current_fields{idx};
        first_underscore_pos = find(current_fieldname == '_', 1);
        if isempty(first_underscore_pos)
            current_phase = current_fieldname;
        else
            current_phase = current_fieldname(1:first_underscore_pos-1);
        end
        current_phase(1) = upper(current_phase(1));
        both_fields_values.(current_phase) = [experiment_results.(current_fieldname)];
    end
    
    % Find title
    if isempty(first_underscore_pos)
        current_field_for_label = '';
    else
        current_field_for_label = current_fieldname(find(current_fieldname == '_', 1)+1:end);
        current_field_for_label = strrep(current_field_for_label, '_', ' ');
        current_field_for_label = strrep(current_field_for_label, ' during', '');
    end
end

% Open figure
fig = figure('Color', [1, 1, 1], ...
    'Name', [current_field_for_label ' - Averages'], ...
    'Position', [680, 560, 590, 420]);

% Right panel - y=x figure
scatter_ax = subplot('Position', [0.41, 0.1375, 0.55, 0.78]);
fieldnames_to_display = fieldnames(both_fields_values);
for i=1:numOfUniqueGroups
    each_group_indices{i} = strcmp(relevant_participants_grouping, groups_titles{i});
    scatter(both_fields_values.(fieldnames_to_display{1})(each_group_indices{i}), both_fields_values.(fieldnames_to_display{2})(each_group_indices{i}), 50, 'o', 'filled', 'MarkerFaceAlpha', 0.7, 'CData', groups_colors{i});
    hold on;
end

if is_displaying_sleep_vs_wake
    xlabel_str = [current_prefix, 'sleep'];
    ylabel_str = [current_prefix, 'wake'];
    xlabel_str(1) = upper(xlabel_str(1));
    ylabel_str(1) = upper(ylabel_str(1));
    bar_labels = {'Sleep', 'Wake'};
else
    xlabel_str = fieldnames_to_display{1};
    ylabel_str = fieldnames_to_display{2};
    bar_labels = {xlabel_str, ylabel_str};
end
xlabel(xlabel_str);
ylabel(ylabel_str);
if mean(both_fields_values.(fieldnames_to_display{1}) < both_fields_values.(fieldnames_to_display{2})) < 0.6
    legend_pos = 'NorthWest';
else
    legend_pos = 'SouthEast';
end
leg_obj = legend(groups_titles{:}, 'AutoUpdate', 'off', 'Location', legend_pos);
leg_obj.ItemTokenSize(1) = 12;
scatter_ax.FontSize = 14;

% Set rectangluar axis
switch current_field_for_label
    case 'Average Interval length'
        ylim(scatter_ax, [0, 500]);
    case 'Mean amplitude LI'
        ylim(scatter_ax, [0, 1]);
end
if strcmp(xlabel_str, 'BPM') && strcmp(ylabel_str, 'IPPM')
    current_ylim = ylim(scatter_ax);
    ylim(scatter_ax, [0, current_ylim(2)]);
end  
current_axis = axis;
rectangalur_axis = repmat([min(current_axis), max(current_axis)], [1, 2]);
axis(rectangalur_axis);

% Add y=x line
yx_line = linspace(rectangalur_axis(1), rectangalur_axis(2), 3);
plot(yx_line, yx_line, 'k:', 'LineWidth', 2);

% Left panel - bar graph
bar_ax = subplot('Position', [0.1, 0.1375, 0.21, 0.78]);
bar_width = 0.3;
bar_objects = cell(1, numOfUniqueGroups);
for group_index = 1:numOfUniqueGroups
    mean_values = nan(2, 1);
    current_group_indices = each_group_indices{group_index};

    for condition_index = 1:2
        condition_field = fieldnames_to_display{condition_index};
        current_measurements = both_fields_values.(condition_field)(current_group_indices);
        current_x_offsets = x_offsets_relevant_participants(current_group_indices)';
        current_measurements_std = nanstd(current_measurements);
        current_measurements_ste = current_measurements_std/sqrt(numel(current_measurements));
        mean_values(condition_index) = nanmean(current_measurements);
        x_value = condition_index + (group_index-1.5)*bar_width;
        bar_objects{group_index} = bar(x_value, mean_values(condition_index), bar_width, 'LineWidth', 2, 'FaceColor', groups_colors{group_index}, ...
            'EdgeAlpha', 0, 'FaceAlpha', 0.8);
        hold on;
        scatter(repmat(x_value, size(current_measurements)) + current_x_offsets, current_measurements, 20, 'o', 'filled', 'MarkerFaceAlpha', 0.5, 'CData', groups_colors{group_index}, 'MarkerEdgeColor', [0,0,0]);
        errorbar(x_value, nanmean(current_measurements), current_measurements_ste, 'LineWidth', 2, 'Color', 'k');
        
        fprintf('For %s, group %s, condition %s: %1.1f +- %1.1f (Median: %1.1f)\n', ...
            current_field_for_label, groups_titles{group_index}, condition_field, ...
            mean_values(condition_index), current_measurements_std, nanmedian(current_measurements));
    end
    
    ratio_between_conditions = both_fields_values.(fieldnames_to_display{2})(current_group_indices) ./ both_fields_values.(fieldnames_to_display{1})(current_group_indices);
    ratio_between_conditions_std = nanstd(ratio_between_conditions);
    fprintf('For %s, group %s, ratio between conditions: %1.3f +- %1.3f (Median: %1.3f)\n', ...
        current_field_for_label, groups_titles{group_index}, ...
        nanmean(ratio_between_conditions), ratio_between_conditions_std, nanmedian(ratio_between_conditions));
end
ylim(rectangalur_axis(1:2));
xticks([1, 2]);
xlim([0.5, 2.5]);
xticklabels(bar_labels);
switch current_field_for_label
    case 'RatePerMinute'
        ylabel('BPM');
    case 'Respiration peaks count avg'
        ylabel('IPPM');
    case 'Mean LI'
        ylabel('Mean LI');
    case 'Mean amplitude LI'
        ylabel('Mean LI Amplitude');
    case 'Nostril corr'
        ylabel('Inter nostril correlation');
    case 'Average Interval length'
        ylabel('Mean Interval Length');
    case ''
    otherwise
        fprintf('Missing y-label for field: %s\n', current_field_for_label);
end

bar_ax.FontSize = 14;
bar_ax.Box = 'off';

set([fig.Children], 'FontName', 'Calibri');
set([fig.Children], 'FontWeight', 'bold');
set([fig.Children], 'LineWidth', 2);
leg_obj.LineWidth = 0.5;

%% Fine-tuning of positions for ylabels and axis limits

switch current_field_for_label
    case 'Respiration peaks count avg'
        nan;
    case 'RatePerMinute'
        bar_ax.YLabel.Position(1) = bar_ax.YLabel.Position(1) + 0.3;
    case 'Average Interval length'
        scatter_ax.YLabel.Position(1:2) = scatter_ax.YLabel.Position(1:2) + [23, -50];
        bar_ax.YLabel.Position(1:2) = bar_ax.YLabel.Position(1:2) + [0.1, -50];
        ylim([bar_ax, scatter_ax], [0, 500]);
    case 'Nostril corr'
        scatter_ax.YLabel.Position(1) = scatter_ax.YLabel.Position(1) + 0.1;
        bar_ax.YLabel.Position(1) = bar_ax.YLabel.Position(1) + 0.12;
    case 'Mean amplitude LI'
        bar_ax.YLabel.Position(1) = bar_ax.YLabel.Position(1) + 0.05;
        ylim([bar_ax, scatter_ax], [0, 1]);
    case 'Mean LI'
        scatter_ax.YLabel.Position(1) = scatter_ax.YLabel.Position(1) + 0.1;
        bar_ax.YLabel.Position(1) = bar_ax.YLabel.Position(1) + 0.35;
    case 'PercentBreathsWithInhalePause'
        scatter_ax.YLabel.Position(1) = scatter_ax.YLabel.Position(1) + 1;
    case 'COV InhaleVolume'
        scatter_ax.YLabel.Position(1) = scatter_ax.YLabel.Position(1) + 0.01;
    case 'Exhale value'
        scatter_ax.YLabel.Position(1) = scatter_ax.YLabel.Position(1) + 0.03;
    otherwise
        fprintf('Missing fine-tuning of positions for field: %s\n', current_field_for_label);
end

