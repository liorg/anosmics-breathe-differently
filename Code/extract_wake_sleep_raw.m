function [Data_wake, Data_sleep] =extract_wake_sleep_raw(CurrentResp,timings_per_subj,CutEdge,ExcludeNap, samplingRate)

if ~exist('samplingRate', 'var')
    samplingRate = 6;
end

% Start_sleep=timings_per_subj.night;
% Wake_up=timings_per_subj.morning;
% NapStart=timings_per_subj.nap_start;
% NapEnd=timings_per_subj.nap_end;

Start_sleep=timings_per_subj.Start_sleep;
Wake_up=timings_per_subj.Wake_up;
NapStart=timings_per_subj.NapStart;
NapEnd=timings_per_subj.NapEnd;

if Wake_up > size(CurrentResp, 1) && Start_sleep > size(CurrentResp, 1)
    if Start_sleep < Wake_up % starting at wake, and all data before sleeping is corrupted
        Start_sleep = 1;
        Wake_up = 1;
    else % Starting at sleep, and all data before waking up is corrupted
        Start_sleep = 1;
        Wake_up = size(CurrentResp, 1)+1;
    end
elseif Wake_up > size(CurrentResp, 1)
    Wake_up = size(CurrentResp, 1);
end
if Start_sleep > size(CurrentResp, 1)
    Start_sleep = size(CurrentResp, 1);
end

if isnan(NapStart)
    if Start_sleep<=Wake_up
        if size(CurrentResp,1)-Wake_up<CutEdge*60*60*samplingRate
            Data_wake=CurrentResp(1:Start_sleep,:);
        else
            Data_wake=CurrentResp([1:Start_sleep Wake_up:end],:);
        end
        Data_sleep=CurrentResp(Start_sleep:Wake_up-1,:);
    else % Sleeping, then Wake_up, then wake until Start_sleep
        Data_wake = CurrentResp(Wake_up:Start_sleep,:);
        samples_sleeping_start = Wake_up;
        samples_sleeping_end = size(CurrentResp,1) - Start_sleep;
        if samples_sleeping_start > samples_sleeping_end
            Data_sleep = CurrentResp(1:samples_sleeping_start, :);
        else
            Data_sleep = CurrentResp(Start_sleep:end, :);
        end
    end
elseif ~isnan(NapStart) && Start_sleep<Wake_up  && NapEnd<Start_sleep
    Data_wake=CurrentResp([1:NapStart NapEnd:Start_sleep Wake_up:end],:);
    if ExcludeNap
        Data_sleep=CurrentResp(Start_sleep:Wake_up,:);
    else
        Data_sleep=CurrentResp([NapStart:NapEnd Start_sleep:Wake_up],:);
    end
elseif ~isnan(NapStart) && Start_sleep<Wake_up  && NapStart>Wake_up
    if length(CurrentResp)-NapEnd<CutEdge*60*60*samplingRate
        Data_wake=CurrentResp([1:Start_sleep Wake_up:NapStart],:);
    else
        Data_wake=CurrentResp([1:Start_sleep Wake_up:NapStart NapEnd:end],:);
    end
    if ExcludeNap
        Data_sleep=CurrentResp(Start_sleep:Wake_up,:);
    else
        Data_sleep=CurrentResp([Start_sleep:Wake_up NapStart:NapEnd],:);
    end
else
    fprintf('check timestamps');
end

end